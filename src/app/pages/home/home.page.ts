import { Component } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { AuthConstants } from 'src/app/config/auth-constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  tipoDeUsuario: string = '';
  constructor(
    private storageService: StorageService,
    private router: Router,
  ) { }

  ionViewWillEnter() {
    this.storageService.get(AuthConstants.TYPE_USER).then(res => {
      this.tipoDeUsuario = res;
      console.log(this.tipoDeUsuario);
    });
  }

  logout() {
    this.storageService.clear();
    this.router.navigate(['/']);
  }
}
