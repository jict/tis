import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModalMeritosPage } from './modal-meritos.page';

describe('ModalMeritosPage', () => {
  let component: ModalMeritosPage;
  let fixture: ComponentFixture<ModalMeritosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalMeritosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModalMeritosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
