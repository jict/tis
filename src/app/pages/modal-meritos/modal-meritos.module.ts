import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalMeritosPageRoutingModule } from './modal-meritos-routing.module';

import { ModalMeritosPage } from './modal-meritos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalMeritosPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ModalMeritosPage]
})
export class ModalMeritosPageModule {}
