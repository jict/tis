import { Component, OnInit } from '@angular/core';
import { PuenteService } from 'src/app/services/puente.service';
import { ReqOptionService } from 'src/app/services/req-option.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertController, ModalController } from '@ionic/angular';


@Component({
  selector: 'app-modal-meritos',
  templateUrl: './modal-meritos.page.html',
  styleUrls: ['./modal-meritos.page.scss'],
})
export class ModalMeritosPage implements OnInit {

  idConv:any;
  idUser:any;
  nombre_req:any;
  idInscri:any;

  fileDataForm:FormGroup

  listaDeReqOpcionales:any;

  messageAlert:any;
  constructor(private puenteService:PuenteService,
              private reqOption:ReqOptionService,
              private formBuilder:FormBuilder,
              private alertCtrl:AlertController,
              private modalCtrl:ModalController
    ) {
      this.buildForm();
    this.puenteService.$getObjectSource.subscribe((data:any)=>{
      this.nombre_req = data.name_req
      this.idInscri = data.id_insc_conv;
    }).unsubscribe();
   }

  ngOnInit() {
    this.listarRequisitosOpcionales();
  }
  private buildForm() {
    this.fileDataForm = this.formBuilder.group({
      idFile:[''],
      puntoDoc:[''],
      puntoAniadir:['',[Validators.required]]
    });
  }
  listarRequisitosOpcionales(){
        this.reqOption.listarDocumentosExGeneralPresentados(this.idInscri,this.nombre_req).subscribe(
          res=>{
            if( res == "Empty"){
              console.log(res);
            }else{
              this.listaDeReqOpcionales = res;
              console.log(res);
              
            }
          },error=>console.log(error)
       );
  }

  modificar(idFile,puntoDoc){
    this.fileDataForm.controls["idFile"].setValue(idFile);
    this.fileDataForm.controls["puntoDoc"].setValue(puntoDoc);
    console.log(this.fileDataForm.value);
    this.reqOption.modificarPuntoExperienciaGeneral(this.fileDataForm.value).subscribe(
      res=>{
        if(res == "Succes"){
          console.log(res);
          this.messageAlert= "La puntuación se modificó correctamente";
          this.alert();
          this.doRefresh();
          this.fileDataForm.reset();
        }else{
          console.log(res);
          this.messageAlert= "La puntuación que desea asignarle no coincide con la puntuación por documento";
          this.alert();
        }
      }
    )
  }

  async alert() {
    const alert = await this.alertCtrl.create({
      message: this.messageAlert,
      buttons: ['OK']
    });

    await alert.present();
  }
  doRefresh() {
    console.log('Begin async operation');

    setTimeout(() => {
      this.ngOnInit()
      //event.target.complete();
    }, 2000);
  }
  dismiss(){
    this.modalCtrl.dismiss();
  }
}
