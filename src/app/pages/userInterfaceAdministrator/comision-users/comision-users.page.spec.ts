import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ComisionUsersPage } from './comision-users.page';

describe('ComisionUsersPage', () => {
  let component: ComisionUsersPage;
  let fixture: ComponentFixture<ComisionUsersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComisionUsersPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ComisionUsersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
