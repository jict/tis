import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ComisionUsersPageRoutingModule } from './comision-users-routing.module';

import { ComisionUsersPage } from './comision-users.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComisionUsersPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ComisionUsersPage]
})
export class ComisionUsersPageModule {}
