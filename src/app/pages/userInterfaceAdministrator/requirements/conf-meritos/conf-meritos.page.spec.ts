import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConfMeritosPage } from './conf-meritos.page';

describe('ConfMeritosPage', () => {
  let component: ConfMeritosPage;
  let fixture: ComponentFixture<ConfMeritosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfMeritosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConfMeritosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
