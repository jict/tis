import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PuenteService } from 'src/app/services/puente.service';
import { ReqOptionService } from 'src/app/services/req-option.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-conf-meritos',
  templateUrl: './conf-meritos.page.html',
  styleUrls: ['./conf-meritos.page.scss'],
})
export class ConfMeritosPage implements OnInit {
  seccionForm:FormGroup
  idConv:any;
  messgeAlert:any;
  listaDeSecciones:Array<any>;
  listaDePuntos:any;
  sumaTotal:any;
  constructor(private forBuilder: FormBuilder,
              private puenteServicio: PuenteService,
              private seccion:ReqOptionService,
              private alertCtrl:AlertController) {
    this.buildForm();
    this.puenteServicio.$getObjectSource.subscribe((data:any)=>{
      this.idConv = data;
      console.log(this.idConv);
      this.seccionForm.controls['idConv'].setValue(this.idConv);
    }).unsubscribe();
  }

  ngOnInit() {
    this.listarSecciones();
    this.obtenerSumaTotal();
    this.listarPuntosSecciones();
  }
  private buildForm() {
    this.seccionForm = this.forBuilder.group({
      idConv:[''],
      idSeccion:[''],
      puntos:['', [Validators.required]]
    });
  }
  obtenerSumaTotal(){
    return this.seccion.sumarSecciones(this.idConv).subscribe(
      res=>{
        if(res == "Empty"){
          console.log("La suma es:" + this.sumaTotal);
        }else{
          this.sumaTotal = res;
          console.log("La suma es:" + this.sumaTotal);
        }
      }
    );
  }
  listarSecciones(){
    return this.seccion.listarSecciones().subscribe(
      res=>{
        if(res == "Empty"){
          console.log(res);
        }else{
          this.listaDeSecciones = res;
          console.log(this.listaDeSecciones);
          
        }
      },error=> console.log(error)
      
    );
  }
  listarPuntosSecciones(){
    return this.seccion.listarPuntosSecciones(this.idConv).subscribe(
      res=>{
        if(res == "Empty"){
          console.log(res);
        }else{
          this.listaDePuntos = res;
          console.log(this.listaDePuntos);
          
        }
      },error=> console.log(error)
      
    );
  }
  guardarConfiguracion(evente:Event){
    event.preventDefault();
    console.log(this.seccionForm.value);
    return this.seccion.añadirSeccion(this.seccionForm.value).subscribe(
      res=>{
        if(res=="Succes"){
          this.messgeAlert="Sección añadido correctamente"
          this.seccionForm.controls['name'].reset();
          this.seccionForm.controls['puntos'].reset();
          this.alert();
          this.doRefresh();
        }else{
          this.messgeAlert="El total del porcentaje es igual a 100"
          this.alert();
        }
        
      },error=> console.log(error)
    );
  }
  asignar(idSeccion){
    this.seccionForm.controls['idSeccion'].setValue(idSeccion);
    console.log(this.seccionForm.value);
    this.seccion.añadirSeccion(this.seccionForm.value).subscribe(
      res=>{
        if(res=="Succes"){
          this.messgeAlert="Puntos asignados correctamente"
          this.seccionForm.controls['puntos'].reset();
          this.alert();
          this.doRefresh();
        }else{
          this.seccionForm.controls['puntos'].reset();
          this.messgeAlert="Los puntos acumulados debe ser menor o igual a 100%"
          this.alert();
          this.doRefresh();
        }
        
      },error=> console.log(error)
    );
    
  }
  async alert() {
    const alert = await this.alertCtrl.create({
      message: this.messgeAlert,
      buttons: ['OK']
    });

    await alert.present();
  }
  doRefresh() {
    console.log('Begin async operation');

    setTimeout(() => {
      this.ngOnInit()
      //event.target.complete();
    }, 2000);
  }
  sendIdConv(){
    this.puenteServicio.sendObjectSource(this.idConv)
    console.log(this.idConv)
  }
}
