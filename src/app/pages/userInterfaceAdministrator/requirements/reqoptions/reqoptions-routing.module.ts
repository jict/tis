import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReqoptionsPage } from './reqoptions.page';

const routes: Routes = [
  {
    path: '',
    component: ReqoptionsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReqoptionsPageRoutingModule {}
