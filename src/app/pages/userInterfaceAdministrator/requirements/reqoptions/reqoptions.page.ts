import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PuenteService } from 'src/app/services/puente.service';
import { ReqOptionService } from 'src/app/services/req-option.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-reqoptions',
  templateUrl: './reqoptions.page.html',
  styleUrls: ['./reqoptions.page.scss'],
})
export class ReqoptionsPage implements OnInit {
  reqOptionForm: FormGroup
  deleteReqOptionForm:FormGroup
  rendimientoForm:FormGroup;
  listaDeSecciones:any;
  messgeAlert:any;
  idConv:any;
  reqOptions:any;
  listaRendi:Array<any>;
  sumaTotalRendimiento:any;
  sumaTotalExperiencia:any;
  get name() {
    return this.reqOptionForm.get('name')
  }
  get point() {
    return this.reqOptionForm.get('point')
  }
  get pointMax() {
    return this.reqOptionForm.get('pointMax')
  }
  get nombre() {
    return this.rendimientoForm.get('name')
  }
  get puntos() {
    return this.rendimientoForm.get('point')
  }
  public errorsMessages = {
    name: [
      { type: 'required', message: 'Nombre es requerido' },
      { type: 'maxlength', message: 'Maximo 200 caracteres alfabeticos' },
      { type: 'minlength', message: 'Minimo 3 caracteres alfabeticos' },
      { type: 'pattern', message: 'Solo caracteres alfabeticos' }
    ],
    point: [
      { type: 'required', message: 'Puntos es requerido' },
      { type: 'max', message: 'Solo se admite 1 caracter numerico' },
      { type: 'pattern', message: 'Solo se permite caracteres alfabeticos' }
    ],
    puntos: [
      { type: 'required', message: 'Puntos es requerido' },
      { type: 'max', message: 'Solo se admite 2 caracter numerico' },
      { type: 'pattern', message: 'Solo se permite caracteres alfabeticos' }
    ],
    pointMax: [
      { type: 'required', message: 'Puntos maximos es requerido' },
      { type: 'max', message: 'El valor maximo es 20' },
      { type: 'pattern', message: 'Solo se permite caracteres alfabeticos' }
    ]
  } 

  constructor(private formBuilder:FormBuilder,
              private puenteService:PuenteService,
              private reqOption:ReqOptionService,
              private router:Router,
              private alertCtrl:AlertController) {
    this.buildForm()
    this.buildFormRendimiento();     
    this.buildFormDelete()       
    this.puenteService.$getObjectSource.subscribe((data:any)=>{
      this.idConv = data
      console.log(this.idConv)
      this.reqOptionForm.controls['idConv'].setValue(this.idConv)
      this.rendimientoForm.controls['idConv'].setValue(this.idConv)
    }).unsubscribe();

   }
  ngOnInit() {
    this.listReqOption();
    this.listarRendimientosAcademicos();
    this.listarSecciones();
    this.obtenerSumaTotalRendimiento();
    this.obtenerSumaTotalExperiencia();
  }
  private buildForm() {
    this.reqOptionForm = this.formBuilder.group({
      idConv:['',[Validators.required]],
      name:['',[Validators.required,Validators.maxLength(200), Validators.minLength(3), Validators.pattern('^[a-zA-ZÀ-ÿ\u00f1\u00d1,0-9-() ]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1,0-9-() ]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1,0-9-() ]+$')]],
      point:['',[Validators.required,Validators.max(9)]],
      pointMax:['',[Validators.required,Validators.max(100)]],
      idSeccion:['2'],
    })
  }
  private buildFormRendimiento() {
    this.rendimientoForm = this.formBuilder.group({
      idConv:[''],
      name:['',[Validators.required,Validators.maxLength(200), Validators.minLength(3), Validators.pattern('^[a-zA-ZÀ-ÿ\u00f1\u00d1,0-9-() ]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1,0-9-() ]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1,0-9-() ]+$')]],
      point:['', [Validators.required,Validators.max(100)]],
      idSeccion:['1'],
    })
  }
  private buildFormDelete(){
    this.deleteReqOptionForm = this.formBuilder.group({
      idReq:['']
    })
  }
  obtenerSumaTotalRendimiento(){
    return this.reqOption.sumarRendimiento(this.idConv).subscribe(
      res=>{
        if(res == "Empty"){
          console.log("La suma es:" + this.sumaTotalRendimiento);
        }else{
          this.sumaTotalRendimiento = res;
          console.log("La suma es:" + this.sumaTotalRendimiento);
        }
      }
    );
  }
  obtenerSumaTotalExperiencia(){
    return this.reqOption.sumarExperiencia(this.idConv).subscribe(
      res=>{
        if(res == "Empty"){
          console.log("La suma es:" + this.sumaTotalExperiencia);
        }else{
          this.sumaTotalExperiencia = res;
          console.log("La suma es:" + this.sumaTotalExperiencia);
        }
      }
    );
  }
  listarSecciones(){
    return this.reqOption.listarSecciones().subscribe(
      res=>{
        if(res == "Empty"){
          console.log(res);
        }else{
          this.listaDeSecciones = res;
        }
      },error=> console.log(error)
      
    );
  }
  submit(event:Event){
    console.log(this.reqOptionForm.value);
    this.reqOption.addReqOption(this.reqOptionForm.value).subscribe(
      res=>{
        if(res=="Succes"){
          this.messgeAlert="Experiencia General añadido correctamente"
          this.reqOptionForm.controls['name'].reset()
          this.reqOptionForm.controls['point'].reset()
          this.reqOptionForm.controls['pointMax'].reset()
          this.alert();
          this.doRefresh();
        }else{
          this.messgeAlert="El puntaje que quiere asignar sobrepasa el máximo configurado previamente"
          this.alert();
        }
      },
      error=>console.log(error)
    )
  }
  listReqOption(){
    return this.reqOption.listReqOption(this.idConv).subscribe(
      res=>{
        if(res == "Empty"){
          console.log(res);
        }else{
          this.reqOptions = res
        }
      },
      error=> console.log(error)
    )
  }
  listarRendimientosAcademicos(){
    return this.reqOption.listarRendimientos(this.idConv).subscribe(
      res=>{
        if(res == "Empty"){
          console.log(res);
          
        }else{
          this.listaRendi = res;
          console.log(this.listaRendi);
          
        }
      }
    );
  }
  async alert() {
    const alert = await this.alertCtrl.create({
      message: this.messgeAlert,
      buttons: ['OK']
    });

    await alert.present();
  }
  doRefresh() {
    console.log('Begin async operation');

    setTimeout(() => {
      this.ngOnInit()
      //event.target.complete();
    }, 2000);
  }
  deleteReqOption(idReqOp:any){
    this.deleteReqOptionForm.controls['idReq'].setValue(idReqOp)
    console.log(idReqOp);
    console.log(this.deleteReqOptionForm.value);
    return this.reqOption.delReqOption(this.deleteReqOptionForm.value).subscribe(
      res=>{
        console.log(res);
        this.doRefresh();
      },error=>console.log(error)
    )
  }

  guardarRendmiento(event:Event){
    //console.log("Se presiono el boton para añadir rendimientos academicos");
    return this.reqOption.guardarRendimiento(this.rendimientoForm.value).subscribe(
      res=>{
         if(res=="Succes"){
          this.messgeAlert="Rendimiendo academico añadido correctamente"
          this.rendimientoForm.controls['name'].reset();
          this.rendimientoForm.controls['point'].reset();
          this.alert();
          this.doRefresh();
        }else{
          this.messgeAlert="El porcentaje que quiere asignar sobrepasa el máximo configurado previamente"
          this.alert();
        }
      }
    );
  }
  eliminarRendimiento(id){
    this.deleteReqOptionForm.controls['idReq'].setValue(id)
    return this.reqOption.eliminarRendimiento(this.deleteReqOptionForm.value).subscribe(
      res=>{
        console.log(res);
        this.doRefresh();
      },error=>console.log(error)
    );
  }
  goToHome(){
    this.router.navigate(['/home/listconvocatoryAdmin'])
  }

}
