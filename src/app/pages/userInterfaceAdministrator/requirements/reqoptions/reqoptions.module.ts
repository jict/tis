import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReqoptionsPageRoutingModule } from './reqoptions-routing.module';

import { ReqoptionsPage } from './reqoptions.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReqoptionsPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ReqoptionsPage]
})
export class ReqoptionsPageModule {}
