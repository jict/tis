import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CreateConvocatoryService } from 'src/app/services/create-convocatory.service';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-createconvocatory',
  templateUrl: './createconvocatory.page.html',
  styleUrls: ['./createconvocatory.page.scss'],
})
export class CreateconvocatoryPage implements OnInit {
  convocatoryForm:FormGroup
  dateLimit:Date = new Date()
  get nameConv() {
    return this.convocatoryForm.get('nameConv')
  }
  public errorsMessages = {
    nameConv: [
      { type: 'required', message: 'Nombre es requerido' },
      { type: 'maxlength', message: 'Maximo 200 caracteres' },
      { type: 'minlength', message: 'Minimo 10 caracteres' },
      { type: 'pattern', message: 'Solo se permite caracteres alfabeticos' }
    ]
  }
  constructor(private formbuilder:FormBuilder,
              private createConvocatory:CreateConvocatoryService,
              private alertCtrl:AlertController,
              private router:Router) {
    this.buildForm()
  }

  ngOnInit() {
  }
  private buildForm() {
    this.convocatoryForm = this.formbuilder.group({
    nameConv: ['',  [Validators.required,Validators.maxLength(200), Validators.minLength(10), Validators.pattern('^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1 ]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$')]],
    facultad: ['',  [Validators.required]],
    carrera: ['',  [Validators.required]],
    departamento: ['',  [Validators.required]],
    dateActual: ['', [Validators.required]],
    dateLimit: ['', [Validators.required]],
  });
  /* VISTA ADMINISTRADOR CREAR CONVOCATORIA 
  this.convocatoryForm.valueChanges
    .subscribe(value => {
      console.log(value);
    });*/
  }
  async submit(event: Event){
    event.preventDefault();
    this.createConvocatory.create(this.convocatoryForm.value).subscribe(
      res=>{
        if(res = "Succes"){
          this.alert();
          this.convocatoryForm.reset();
          this.router.navigate(['/home/listconvocatoryAdmin'])
        }
      },
      error => console.log(error)
    );
    console.log(this.convocatoryForm.value);
  }
  async alert() {
    const alert = await this.alertCtrl.create({
      message: 'Convocatoria creada correctamente',
      buttons: ['OK']
    });

    await alert.present();
  }
}
