import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListconvocatoryPageRoutingModule } from './listconvocatory-routing.module';

import { ListconvocatoryPage } from './listconvocatory.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListconvocatoryPageRoutingModule
  ],
  declarations: [ListconvocatoryPage]
})
export class ListconvocatoryPageModule {}
