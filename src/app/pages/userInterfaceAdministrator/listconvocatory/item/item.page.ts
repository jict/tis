import { Component, OnInit } from '@angular/core';
import { PuenteService } from 'src/app/services/puente.service';
import { ListitemsService } from 'src/app/services/listitems.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.page.html',
  styleUrls: ['./item.page.scss'],
})
export class ItemPage implements OnInit {

  /*Variables que almacenan la respuesta enviada desde el backend en caso 
    de que la respuesta sea un array de objetos*/
  oneConvocatory:any; // almacena un array que contiene solo un elemento (informacion de una sola convocatoria)
  listItemsConvo:any; // almacena un array que contiene varios elementos (lista de items)

  idConv:any;
  hiddenMessage: boolean = false;
  messageEmptyList:String = "";

  constructor(private puenteServcio:PuenteService,
              private item:ListitemsService) {
    this.puenteServcio.$getObjectSource.subscribe((data:any)=>{
      this.idConv = data
      console.log(this.idConv)
    }).unsubscribe();
   }

  ngOnInit() {
    this.listarItems();
  }
  listarItems(){
    return this.item.getItems(this.idConv).subscribe(
      res=>{
        if( res == "Empty"){
          console.log(res);
        }else{
          this.listItemsConvo = res;
          console.log(this.listItemsConvo); 
        }  
      }
    )
  }
  sendIdItem(idItem){
    this.puenteServcio.sendObjectSource(idItem)
  }
}
