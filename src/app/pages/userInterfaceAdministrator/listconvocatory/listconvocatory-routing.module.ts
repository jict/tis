import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListconvocatoryPage } from './listconvocatory.page';

const routes: Routes = [
  {
    path: '',
    component: ListconvocatoryPage
  }, 
  {
    path: 'createitem',
    loadChildren: () => import('../../userInterfaceAdministrator/createitem/createitem.module').then( m => m.CreateitemPageModule)
  },
  {
    path:'requirements',
    loadChildren:() => import('../../userInterfaceAdministrator/requirements/requirements.module').then(m => m.RequirementsPageModule)
  },
  {
    path: 'item',
    loadChildren: () => import('../../userInterfaceAdministrator/listconvocatory/item/item.module').then( m => m.ItemPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListconvocatoryPageRoutingModule {}
