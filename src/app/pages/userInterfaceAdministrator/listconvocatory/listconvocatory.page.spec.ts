import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListconvocatoryPage } from './listconvocatory.page';

describe('ListconvocatoryPage', () => {
  let component: ListconvocatoryPage;
  let fixture: ComponentFixture<ListconvocatoryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListconvocatoryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListconvocatoryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
