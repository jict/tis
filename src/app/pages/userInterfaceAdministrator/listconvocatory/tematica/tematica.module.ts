import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TematicaPageRoutingModule } from './tematica-routing.module';

import { TematicaPage } from './tematica.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TematicaPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [TematicaPage]
})
export class TematicaPageModule {}
