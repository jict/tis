import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TematicaPage } from './tematica.page';

describe('TematicaPage', () => {
  let component: TematicaPage;
  let fixture: ComponentFixture<TematicaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TematicaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TematicaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
