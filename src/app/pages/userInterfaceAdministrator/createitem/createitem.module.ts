import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateitemPageRoutingModule } from './createitem-routing.module';

import { CreateitemPage } from './createitem.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateitemPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [CreateitemPage]
})
export class CreateitemPageModule {}
