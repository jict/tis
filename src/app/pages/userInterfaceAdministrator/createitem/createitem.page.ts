import { Component, OnInit } from '@angular/core';
import { PuenteService } from 'src/app/services/puente.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CreateItemService } from 'src/app/services/create-item.service';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-createitem',
  templateUrl: './createitem.page.html',
  styleUrls: ['./createitem.page.scss'],
})
export class CreateitemPage implements OnInit {
  idConv:any
  itemForm:FormGroup
  get name() {
    return this.itemForm.get('name')
  }
  get hrWork(){
    return this.itemForm.get('hrWork')
  }
  get itRequired(){
    return this.itemForm.get('itRequerid')
  }
  public errorsMessages = {
    name: [
      { type: 'required', message: 'Nombre es requerido' },
      { type: 'maxlength', message: 'Maximo 200 caracteres' },
      { type: 'minlength', message: 'Minimo 10 caracteres' },
      { type: 'pattern', message: 'Solo se permite caracteres alfabeticos' }
    ],
    hrWork: [
      { type: 'required', message: 'Horas de trabajo es requerido' },
      { type: 'maxlength', message: 'Maximo 2 caracteres' },
      { type: 'pattern', message: 'Solo se permite caracteres numericos' }
    ],
    itRequerid: [
      { type: 'required', message: 'Nro de Ítems es requerido' },
      { type: 'maxlength', message: 'Maximo 2 caracteres' },
      { type: 'pattern', message: 'Solo se permite caracteres numericos'}
    ],  
  }
  constructor(private puenteServcio:PuenteService,
              private formBuilder: FormBuilder,
              private createItem:CreateItemService,
              private alertCtrl:AlertController,
              private router:Router) { 
    this.buildForm()
    this.puenteServcio.$getObjectSource.subscribe((data:any)=>{
      this.idConv = data
      console.log(this.idConv)
      this.itemForm.controls['idConv'].setValue(this.idConv)
    }).unsubscribe();
  }

  ngOnInit() {
  }
  private buildForm() {
    this.itemForm = this.formBuilder.group({
      idConv:['',[Validators.required]],
      name:['',[Validators.required,Validators.maxLength(200), Validators.minLength(10), Validators.pattern('^[a-zA-ZÀ-ÿ\u00f1\u00d1,0-9-() ]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1,0-9-() ]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1,0-9-() ]+$')]],
      hrWork:['',[Validators.required,Validators.maxLength(2),Validators.pattern('[0-9]*')]],
      itRequerid:['',[Validators.required,Validators.maxLength(2),Validators.pattern('[0-9]*')]],
    })
  }
  submit(event:Event){
    console.log(this.itemForm.value);
    this.createItem.create(this.itemForm.value).subscribe(
      res=>{
        if(res = "Succes"){
          this.alert()
          this.itemForm.reset()
          this.router.navigate(['/home/listconvocatoryAdmin']);
        }
      },
      error=> console.log(error)
    );
  }
  async alert(){
    const alert = await this.alertCtrl.create({
      message: 'Item creado correctamente',
      buttons: ['OK']
    });
    await alert.present();
  }
}
