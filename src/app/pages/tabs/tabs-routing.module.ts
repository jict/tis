import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children:[
    {
      path: 'createconvocatory',
      loadChildren: () => import('../userInterfaceAdministrator/createconvocatory/createconvocatory.module').then( m => m.CreateconvocatoryPageModule)
    },{
      path: 'listconvocatoryAdmin',
      loadChildren: () => import('../userInterfaceAdministrator/listconvocatory/listconvocatory.module').then( m => m.ListconvocatoryPageModule)  
    },
    {
      path: 'postulaciones',
      loadChildren: () => import('../userInterfacePostulante/postulaciones/postulaciones.module').then( m => m.PostulacionesPageModule)
    },
    {
      path: 'listconvocatoryPost',
      loadChildren: () => import('../userInterfacePostulante/listconvocatory/listconvocatory.module').then( m => m.ListconvocatoryPageModule)
    },
    {
      path: 'generatecode',
      loadChildren: () => import('../userInterfaceSeretaria/generatecode/generatecode.module').then( m => m.GeneratecodePageModule)
    },
    {
      path: 'listconvocatorias',
      loadChildren: () => import('../userInterfaceSeretaria/listconvocatorias/listconvocatorias.module').then( m => m.ListconvocatoriasPageModule)
    },
    {
      path: 'listusers',
      loadChildren: () => import('../userInterfaceSeretaria/listusers/listusers.module').then( m => m.ListusersPageModule)
    },
    {
      path: 'public-convocatory',
      loadChildren: () => import('../userInterfaceSeretaria/public-convocatory/public-convocatory.module').then( m => m.PublicConvocatoryPageModule)
    },
    {
      path: 'comision-users',
      loadChildren: () => import('../userInterfaceAdministrator/comision-users/comision-users.module').then( m => m.ComisionUsersPageModule)
    },
    {
      path: 'convocatorias',
      loadChildren: () => import('../userInterfaceComision/convocatorias/convocatorias.module').then( m => m.ConvocatoriasPageModule)
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
