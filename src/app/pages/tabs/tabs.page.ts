import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { AuthConstants } from 'src/app/config/auth-constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {
  codAdministrador: Boolean = false;
  codPostulante: Boolean = false;
  codSecretaria:Boolean = false;
  codComision: Boolean = false;

  sectionSecretary = [
    {label:'Convocatorias',path:'listconvocatorias',icon:'albums'},
    {label:'Calificaciones',path:'public-convocatory',icon:'add-circle'},
    {label:'Generar Codigo',path:'generatecode',icon:'terminal'}
  ]
  sectionAdmin =  [
    {label:'Convocatorias',path:'listconvocatoryAdmin',icon:'albums'},
    {label:'Crear convocatoria',path:'createconvocatory',icon:'add-circle'},
    {label:'Crear Usuario Comision',path:'comision-users',icon:'person-circle'},
    {label:'Calificaciones',path:'public-convocatory',icon:'add-circle'},
  ]
  sectionPostul =  [
    {label:'Convocatorias',path:'listconvocatoryPost',icon:'albums'},
    {label:'Calificaciones',path:'public-convocatory',icon:'person-circle'},
  ]
  sectionComition =  [
    {label:'Convocatorias',path:'convocatorias',icon:'albums'},
    {label:'Calificaciones',path:'public-convocatory',icon:'add-circle'},
  ]

  tipoDeUsuario: string = '';
  constructor(private storageService:StorageService,
              private router:Router) {
                this.storageService.get(AuthConstants.TYPE_USER).then(res => {
                  switch (res) {
                    case 'Postulante': this.codPostulante = true; break;
                    case 'Secretaria': this.codSecretaria = true; break;
                    case 'Comision': this.codComision = true; break;
                    case 'Administrador': this.codAdministrador = true; break;
                    default: break;
                  }
                });
               }

  ngOnInit() {
  }
  ionViewWillEnter() {
    this.storageService.get(AuthConstants.TYPE_USER).then(res => {
      this.tipoDeUsuario = res;
      console.log(this.tipoDeUsuario);
    });
  }

  logout() {
    this.storageService.clear();
    this.router.navigateByUrl("");
    
  }

}
