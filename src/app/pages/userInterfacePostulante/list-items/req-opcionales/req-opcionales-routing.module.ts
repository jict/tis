import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReqOpcionalesPage } from './req-opcionales.page';

const routes: Routes = [
  {
    path: '',
    component: ReqOpcionalesPage
  },
  {
    path: 'list-items',
    loadChildren: () => import('../../list-items/list-items.module').then( m => m.ListItemsPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReqOpcionalesPageRoutingModule {}
