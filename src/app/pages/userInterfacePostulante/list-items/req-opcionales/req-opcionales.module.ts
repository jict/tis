import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReqOpcionalesPageRoutingModule } from './req-opcionales-routing.module';

import { ReqOpcionalesPage } from './req-opcionales.page';
import { FileUploadModule } from '@iplab/ngx-file-upload';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReqOpcionalesPageRoutingModule,
    FileUploadModule,
    ReactiveFormsModule
  ],
  declarations: [ReqOpcionalesPage]
})
export class ReqOpcionalesPageModule {}
