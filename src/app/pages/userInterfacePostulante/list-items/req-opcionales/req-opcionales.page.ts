import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReqOptionService } from 'src/app/services/req-option.service';
import { PuenteService } from 'src/app/services/puente.service';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/services/storage.service';
import { AuthConstants } from 'src/app/config/auth-constants';
import { FileUploadService } from 'src/app/services/file-upload.service';
import { AngularFireUploadTask, AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { UploadTaskSnapshot } from '@angular/fire/storage/interfaces';

@Component({
  selector: 'app-req-opcionales',
  templateUrl: './req-opcionales.page.html',
  styleUrls: ['./req-opcionales.page.scss'],
})
export class ReqOpcionalesPage implements OnInit {
  listaReqOptions: Array<any>;
  listaRendimiento: Array<any>;
  idConv: any;
  idItem: any;
  idUser: any;
  messageAlert: any;
  formulario: FormGroup;
  sendData: FormGroup;
  auxForm: FormGroup;
  shortnamesArray = new Array();
  failFormat = new Array();
  files = new Array();
  puntos = new Array();
  downloadUrl: string;

  constructor(
    private reqOption: ReqOptionService,
    private puenteService: PuenteService,
    private formBuilder: FormBuilder,
    private router: Router,
    private storageService: StorageService,
    private uploadService: FileUploadService,
    private storage: AngularFireStorage
  ) {
    this.puenteService.$getObjectSource.subscribe((data: any) => {
      this.idConv = data;
    }).unsubscribe();
  }
  ngOnInit() {
    this.buildForm();

    this.storageService.get(AuthConstants.AUTH).then(res => {
      this.idUser = res;
      this.auxForm.controls['idConv'].setValue(this.idConv);
      this.auxForm.controls['idUser'].setValue(this.idUser);
      console.log(this.auxForm.value);

      this.uploadService.getIdInscConv(this.auxForm.value).subscribe(res => {
        this.sendData.controls['id_insc_conv'].setValue(res[0].id);
      }, error => console.log(error)
      );
    });
    this.listarRendimientos();
    this.listReqOption();
  }

  listarRendimientos() {
    return this.reqOption.listarRendimientos(this.idConv).subscribe(
      res => {
        if (res == "Empty") {
          console.log(res);
        } else {
          this.listaRendimiento = res;
          //console.log(this.listaRendimiento);
        }
      }, error => console.log(error)
    );
  }

  listReqOption() {
    return this.reqOption.listReqOption(this.idConv).subscribe(
      res => {
        if (res == "Empty") {
          console.log(res);
        } else {
          //console.log(res);
          this.listaReqOptions = res;
        }
        for (let i = 0; i < this.listaReqOptions.length; i++) {
          let seccion = new Array<File>();
          this.files[i] = seccion;
        }
        //console.log(this.files);
        //console.log(this.errForm);
      },
      error => console.log(error)
    )
  }

  goToHome() {
    this.router.navigate(['/home/listconvocatoryPost'])
  }

  EnviarAndGoToItems() {
    console.table(this.listaReqOptions);
    console.table(this.puntos);
    let sumatoria = 0;
    let punto = 0;
    let punto_max = 0;
    let control = 0;
    console.log(sumatoria);
    for (let k = 0; k < this.files.length; k++) {
      for (let l = 0; l < this.files[k].length; l++) {

        let indice = l + 1;
        let file = this.files[k][l];
        let path = 'opcionales/' + this.listaReqOptions[k].nombre + '-User' + this.idUser + '-Conv' + this.idConv + '-Documento-' + indice;
        let uploadTask: AngularFireUploadTask = this.storage.upload(path, file);

        uploadTask.then((uploadSnapshot: UploadTaskSnapshot) => {
          uploadSnapshot.ref.getDownloadURL().then((downloadURL) => {
            this.downloadUrl = downloadURL;
            this.sendData.controls['nameReq'].setValue(this.listaReqOptions[k].nombre);
            this.sendData.controls['file_dir'].setValue(this.downloadUrl);
            this.sendData.controls['puntuacion'].setValue(this.puntos[k][l]);
            console.table(this.sendData.value);
            this.uploadService.setFileDir(this.sendData.value).subscribe(res => {
              console.log(res);
            });
          })
        });
      }
    }
    this.router.navigate(['//home/listconvocatoryPost/req-opcionales/list-items']);
  }

  buildForm() {
    this.auxForm = this.formBuilder.group({
      idUser: ['', [Validators.required]],
      idConv: ['', [Validators.required]]
    });

    this.sendData = this.formBuilder.group({
      id_insc_conv: ['', [Validators.required]],
      nameReq: ['', [Validators.required]],
      file_dir: ['', [Validators.required]],
      tipo: ['opcional', [Validators.required]],
      estado: ['false', []],
      observacion: ['', []],
      puntuacion: ['', []]
    });
  }

  checkFile(e, index) {
    this.puntos[index] = new Array();
    let auxArray = new Array();
    let punto = parseInt(this.listaReqOptions[index].punto);
    let punto_max = parseInt(this.listaReqOptions[index].punto_max);
    let control = Math.trunc(punto_max / punto);
    // console.log(this.puntos);
    for (let i = 0; i < e.target.files.length; i++) {
      if (e.target.files[i].type == "application/pdf") {
        auxArray[i] = false;
        this.files[index][i] = e.target.files[i];

      } else {
        auxArray[i] = true;
      }
      if (i < control) {
        this.puntos[index][i] = punto;
      } else {
        this.puntos[index][i] = 0;
      }
    }
    console.log(this.puntos[index]);
    if (auxArray.includes(true)) {
      this.failFormat[index] = true;
    } else {
      this.failFormat[index] = false;
    }
    auxArray = new Array();
  }
}
