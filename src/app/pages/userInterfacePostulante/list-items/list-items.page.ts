import { Component, OnInit } from '@angular/core';

import { ListitemsService } from 'src/app/services/listitems.service';
import { StorageService } from 'src/app/services/storage.service';
import { AuthConstants } from 'src/app/config/auth-constants';
import { PuenteService } from 'src/app/services/puente.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ListreqService } from 'src/app/services/listreq.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-list-items',
  templateUrl: './list-items.page.html',
  styleUrls: ['./list-items.page.scss'],
})
export class ListItemsPage implements OnInit {
  idConv:any
  listItemsConvo:any;
  messageEmptyList:String;
  hiddenMessage:Boolean=false;
  inscriptionItemForm:FormGroup;
  idUser:any;
  messageAlert:any;
  constructor(private listItems:ListitemsService,
              private puenteServicio:PuenteService,
              private formBuilder:FormBuilder,
              private storageService:StorageService,
              private itemInscription:ListreqService,
              private alertCtrl:AlertController) {
                this.buildFormInscription();
                this.puenteServicio.$getObjectSource.subscribe((data:any)=>{
                    this.idConv = data;
                    this.inscriptionItemForm.controls['idConv'].setValue(this.idConv);
                }).unsubscribe(); 
              }

  ngOnInit() {
    this.listarItemsConvocatoria()
  }
  private buildFormInscription() {
    this.inscriptionItemForm = this.formBuilder.group({
      idUser:[''],
      idConv:[''],
      idItem:['']
    });
  }
  listarItemsConvocatoria() {
      return this.listItems.getItems(this.idConv).subscribe(
      res=>{
        console.log(res);
        if(res != "Empty"){
          this.listItemsConvo = res
        }else{
          this.messageEmptyList = "No Existen Items Creados"
          this.hiddenMessage=true;
        }
      },error=>console.log(error)
    );
    
  }
  inscripcionItem(idItem){
    this.storageService.get(AuthConstants.AUTH).then(
      res=>{
        this.idUser = res;
        //console.log(this.idUser);
        this.inscriptionItemForm.controls['idUser'].setValue(this.idUser);
        this.inscriptionItemForm.controls['idItem'].setValue(idItem);
        //console.log(this.inscriptionItemForm.value);
        return this.itemInscription.inscripcionItem(this.inscriptionItemForm.value).subscribe(
          res=>{
            if(res == "Succes"){
              this.messageAlert="Inscripción a un Ítem correcta"
              this.alert();
            }else{
              this.messageAlert="Ya esta inscrito en este item"
              this.alert();
            }
          }
        );
      }
    );
  }
  async alert() {
    const alert = await this.alertCtrl.create({
      message: this.messageAlert,
      buttons: ['OK']
    });
    await alert.present();
  }

}
