import { Component, OnInit } from '@angular/core';
import { ListConvocatorysService } from 'src/app/services/list-convocatorys.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { InscriptionconvocatoyService } from 'src/app/services/inscriptionconvocatoy.service';
import { AlertController, ModalController } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage.service';
import { AuthConstants } from 'src/app/config/auth-constants';
import { ListinscriptionconvocatoryService } from 'src/app/services/listinscriptionconvocatory.service';
import { ModalParticiparPage } from '../../modal-participar/modal-participar.page';
import { PuenteService } from 'src/app/services/puente.service';

@Component({
  selector: 'app-listconvocatory',
  templateUrl: './listconvocatory.page.html',
  styleUrls: ['./listconvocatory.page.scss'],
})
export class ListconvocatoryPage implements OnInit {
  convocatories: any
  messageListEmpty: string
  emptyMessageListPostulHidden: boolean = false;
  idConvocatory: any
  idUser: any
  inscriptionConvocatoryForm: FormGroup
  listInscriptionConv: any
  messgeAlert: any
  constructor(
    private listConvocatorias: ListConvocatorysService,
    private formbuilder: FormBuilder,
    private inscriptionConvocatory: InscriptionconvocatoyService,
    private alertCtrl: AlertController,
    private storageService: StorageService,
    private listInscription: ListinscriptionconvocatoryService,
    private modalCtrl: ModalController,
    private puenteService:PuenteService
  ) {
    this.buildForm()
  }

  ngOnInit() {
    this.listarConvocatoriasVigentes()
    this.listarConvocatoriasPostuladas()
  }
  ionViewWillEnter() {
    this.storageService.get(AuthConstants.AUTH).then(res => {
      this.idUser = res;
      console.log(this.idUser);
      this.inscriptionConvocatoryForm.controls['idUser'].setValue(this.idUser);
    });
  }
  private buildForm() {
    this.inscriptionConvocatoryForm = this.formbuilder.group({
      idUser: ['', [Validators.required]],
      idConv: ['', [Validators.required]]
    });
  }

  async openModal(idConv: any) {
    event.preventDefault();
    console.log("Items de esta convocatoria con el id:" + idConv);
    this.storageService.store(AuthConstants.ID_CONV, idConv)
    const modal = await this.modalCtrl.create({
      component: ModalParticiparPage,
      componentProps: {
        'idConvocatoria': idConv
      },
      backdropDismiss: false
    });
    return await modal.present();
  }
  sendData(idConv: any) {
    this.storageService.get(AuthConstants.AUTH).then(res => {
      this.idUser = res;
      console.log(this.idUser);
      this.inscriptionConvocatoryForm.controls['idUser'].setValue(this.idUser);
      this.idConvocatory = idConv
      this.inscriptionConvocatoryForm.controls['idConv'].setValue(this.idConvocatory)
      console.log(this.idConvocatory)
      console.log(this.inscriptionConvocatoryForm.value);
      this.inscriptionConvocatory.inscription(this.inscriptionConvocatoryForm.value).subscribe(
        res => {
          console.log(res)
          if (res == "1") {
            this.messgeAlert = "Ya participa en esta convocatoria"
            this.alert();

          } else {
            this.openModal(idConv);
            //this.messgeAlert = "Participacion con exito"
            //this.alert();
            this.inscriptionConvocatoryForm.reset();
            //this.refrescar();
            //window.location.reload()
          }
        }, error => console.log(error)
      );
    });
  }
  async alert() {
    const alert = await this.alertCtrl.create({
      message: this.messgeAlert,
      buttons: ['OK']
    });

    await alert.present();
  }
  listarConvocatoriasVigentes() {
    return this.listConvocatorias.listarConvocatoriasVigentes().subscribe(
      res => {
        console.log(res);

        if (res != "Empty") {
          this.convocatories = res;
        } else {
          this.messgeAlert = "No existen convocatorias vigentes"
        }
      },
      error => console.log(error)
    );
  }
  listarConvocatoriasPostuladas() {
    this.storageService.get(AuthConstants.AUTH).then(res => {
      this.idUser = res;
      console.log(this.idUser);
      return this.listInscription.getList(this.idUser).subscribe(
        res => {
          console.log(res);
          if (res != "Empty") {
            this.listInscriptionConv = res;
          } else {
            this.emptyMessageListPostulHidden = true;
          }
        }, error => console.log(error)
      );
    });
  }
  irOpcionales(idConv: any) {
    //this.openModal(idConv)
    this.puenteService.sendObjectSource(idConv)
    //event.preventDefault();
    console.log("Items de esta convocatoria con el id:" + idConv);
    //this.storageService.store(AuthConstants.ID_CONV, idConv)
  }
  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      this.ngOnInit()
      event.target.complete();
    }, 3000);
  }
  async deleteInscription(idConv) {
    this.inscriptionConvocatoryForm.controls['idConv'].setValue(idConv);
    console.log(this.inscriptionConvocatoryForm.value);
    return await this.listInscription.deleteInscription(this.inscriptionConvocatoryForm.value).subscribe(
      res => {
        console.log(res);
        this.confirmar();
        this.messgeAlert = "Ya no participara más en esta convocatoria";
        this.alert();
        //window.location.reload();
      }
    )
  }
  confirmar(){
    console.log('Begin async operation');

    setTimeout(() => {
      this.ngOnInit()
      window.location.reload();
    }, 3000);
  }
}
