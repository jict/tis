import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListconvocatoryPage } from './listconvocatory.page';

const routes: Routes = [
  {
    path: '',
    component: ListconvocatoryPage
  },
  {
    path: 'req-opcionales',
    loadChildren: () => import('../list-items/req-opcionales/req-opcionales.module').then( m => m.ReqOpcionalesPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListconvocatoryPageRoutingModule {}
