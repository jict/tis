import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { CodigoService } from 'src/app/services/codigo.service';
import { PuenteService } from 'src/app/services/puente.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
	selector: 'app-modal',
	templateUrl: './modal.page.html',
	styleUrls: ['./modal.page.scss'],
})
export class ModalPage implements OnInit {
	tipoDeUsuario;
	postData = {
		codigo: ''
	}

	codigoCtrl = new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(5), Validators.pattern('[a-zA-Z0-9]+')]);

	constructor(
		public modalController: ModalController,
		private router: Router,
		private codigoService: CodigoService,
		private puenteService: PuenteService,
		private toastService: ToastService
	) {
		this.codigoCtrl.valueChanges.pipe(debounceTime(1000)).subscribe(value => {
			console.log(value);
		});
	}

	ngOnInit() {
	}

	dismiss() {
		this.modalController.dismiss({
			'dismissed': true
		});
	}

	goToRegister(event: Event) {
		event.preventDefault();
		this.postData.codigo = this.codigoCtrl.value;
		this.codigoService.sendCodigo(this.postData).subscribe((res: any) => {
			if (res.tipo) {
				this.puenteService.sendObjectSource(res.tipo);
				this.dismiss();
				this.router.navigate(['/signup']);
			} else {
				this.toastService.presentToast(res, 3000);
			}
		});
	}
}
