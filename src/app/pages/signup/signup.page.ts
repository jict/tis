import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { PuenteService } from 'src/app/services/puente.service';

import { RegisterUserService } from 'src/app/services/register-user.service';
import { Usuario } from 'src/app/models/usuario/usuario';
import { ObjectUnsubscribedError } from 'rxjs';



@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  mesageError: String; //variables para mostrar en el html
  hiddenMesage: Boolean = false;

  /*Este campo recibe de el modal de codigo el tipo de usuario del que es el codigo
    Necesito que lo almacenes junto con los demas Campos en la base de datos
    Puedes ponerlo como un input hidden o o un input disabled que ya lleva este valor
    hazlo tú no termino de entender como funciona tu formulario
  */
  tipoDeUsuario: any = {
    id: "",
    tipo: ""
  };
  /*
    la var tipoDeUsuario == res;
    y res es un objeto que me devuelve
    res = {
      tipo: "Postulante o Comision"
      estado: "Activo Usado"
    }
    entonces llamo
    a sus atributos. this.tipoDeUsuario.tipo y this.tipoDeUsuario.estado y se las mando al formulario
  */


  get userName() {
    return this.signupForm.get('userName')
  }
  get tipo() {
    return this.signupForm.get('tipo')
  }
  get estado() {
    return this.signupForm.get('estado')
  }
  get firsName() {
    return this.signupForm.get('firstName')
  }
  get lastNameP() {
    return this.signupForm.get('lastNameP')
  }
  get lastNameM() {
    return this.signupForm.get('lastNameM')
  }
  get password() {
    return this.signupForm.get('password')
  }
  get confirmPassword() {
    return this.signupForm.get('confirmPassword')
  }
  get email() {
    return this.signupForm.get('email')
  }
  get phone() {
    return this.signupForm.get('phone')
  }
  get dir() {
    return this.signupForm.get('dir')
  }

  public errorsMessages = {
    userName: [
      { type: 'required', message: 'Nombre Usuario es requerido' },
      { type: 'maxlength', message: '15 caracteres como maximo' },
      { type: 'minlength', message: '2 caracteres como minimo' }
    ],
    firstName: [
      { type: 'required', message: 'Nombre(s) es requerido' },
      { type: 'maxlength', message: '40 caracteres como maximo' },
      { type: 'minlength', message: '2 caracteres como minimo' },
      { type: 'pattern', message: 'solo se permiten caracteres' }
    ],
    lastNameP: [
      { type: 'required', message: 'Apellido Paterno es requerido' },
      { type: 'maxlength', message: '20 caracteres como maximo' },
      { type: 'minlength', message: '2 caracteres como minimo' },
      { type: 'pattern', message: 'solo se permiten caracteres' }
    ],
    lastNameM: [
      { type: 'required', message: 'Apellido Materno es requerido' },
      { type: 'maxlength', message: '20 caracteres como maximo' },
      { type: 'minlength', message: '2 caracteres como minimo' },
      { type: 'pattern', message: 'solo se permiten caracteres' }
    ],
    password: [
      { type: 'required', message: 'Contraseña es requerido' },
      { type: 'maxlength', message: '8 caracteres como maximo' },
      { type: 'minlength', message: '6 caracteres como minimo' },
    ],
    confirmPassword: [
      { type: 'required', message: 'Contraseña es requerido' }
    ],
    email: [
      { type: 'required', message: 'Correo es requerido' },
      { type: 'email', message: 'Ingrese un correo electronico valido' }
    ],
    phone: [
      { type: 'nullValidator', message: '' },
      { type: 'maxlength', message: '8 digitos como maximo' },
      { type: 'minlength', message: '8 digitos como minimo' },
      { type: 'pattern', message: 'Solo se permiten numeros' }
    ],
    dir: [
      { type: 'nullValidator', message: '' },
      { type: 'maxlength', message: '50 caracteres como maximo' },
      { type: 'minlength', message: ' 6 caracteres como minimo' }
    ]
  }
  hidden = false;

  signupForm = this.formBuilder.group({
    userName: ['', [Validators.required, Validators.maxLength(15), Validators.minLength(2)]],
    firstName: ['', [Validators.required, Validators.maxLength(25), Validators.minLength(2), Validators.pattern('[a-zA-Z ]*')]],
    lastNameP: ['', [Validators.required, Validators.maxLength(20), Validators.minLength(2), Validators.pattern('[a-zA-Z ]*')]],
    lastNameM: ['', [Validators.required, Validators.maxLength(20), Validators.minLength(2), Validators.pattern('[a-zA-Z ]*')]],
    password: ['', [Validators.required, Validators.maxLength(8), Validators.minLength(6)]],
    confirmPassword: ['', [Validators.required]],
    facultad: ['', []],
    carrera: ['', []],
    email: ['', [Validators.email]],
    phone: ['', [Validators.pattern('[0-9]*'), Validators.maxLength(8), Validators.minLength(8)]],
    dir: ['', [Validators.maxLength(50), Validators.minLength(6)]],
    tipo: [''],//Aqui la estoy almacenando y esto envia a la base de datos
  }, { validator: this.checkPasswords })
  /*
  En la base de datos cree dos campos para el tipo y estado
  */
  checkPasswords(group: FormGroup) {
    let contraseña = group.controls.password.value
    let confirmarContraseña = group.controls.confirmPassword.value

    return contraseña === confirmarContraseña ? null : { notSame: true }
  }

  datosUsuario: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private registerUser: RegisterUserService,
    private puenteService: PuenteService,
  ) {

    this.puenteService.$getObjectSource.subscribe((data: any) => {
      console.log(data)
      this.signupForm.controls['tipo'].setValue(data);
    }).unsubscribe();
  }

  /*

  */
  ngOnInit() { }
  submit() {
    console.log(this.signupForm.value)
    this.registerUser.registerUser(this.signupForm.value)
      .subscribe(
        res => {
          console.log(res);

          if (res == "1") {
            this.mesageError = "Nombre de Usuario o Email regitrado"
            this.hiddenMesage = true;
          } else {
            this.datosUsuario = res;
            this.router.navigate(['/login']);
          }

        },
        error => console.log(error));

    this.hiddenMesage = false;
    this.signupForm.reset()

  }
}
