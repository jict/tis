import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PublicConvocatoryPage } from './public-convocatory.page';

const routes: Routes = [
  {
    path: '',
    component: PublicConvocatoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublicConvocatoryPageRoutingModule {}
