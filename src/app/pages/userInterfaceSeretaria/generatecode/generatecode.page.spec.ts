import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GeneratecodePage } from './generatecode.page';

describe('GeneratecodePage', () => {
  let component: GeneratecodePage;
  let fixture: ComponentFixture<GeneratecodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneratecodePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GeneratecodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
