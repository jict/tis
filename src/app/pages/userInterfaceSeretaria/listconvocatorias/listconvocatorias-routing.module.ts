import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListconvocatoriasPage } from './listconvocatorias.page';

const routes: Routes = [
  {
    path: '',
    component: ListconvocatoriasPage
  },
  {
    path: 'listusers',
    loadChildren: () => import('../listusers/listusers.module').then( m => m.ListusersPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListconvocatoriasPageRoutingModule {}
