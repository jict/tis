import { Component, OnInit } from '@angular/core';
import { ListConvocatorysService } from 'src/app/services/list-convocatorys.service';
import { StorageService } from 'src/app/services/storage.service';
import { AuthConstants } from 'src/app/config/auth-constants';

@Component({
  selector: 'app-listconvocatorias',
  templateUrl: './listconvocatorias.page.html',
  styleUrls: ['./listconvocatorias.page.scss'],
})
export class ListconvocatoriasPage implements OnInit {
  convocatorias:any
  messgeAlert:String
  constructor(private listConvocatorias:ListConvocatorysService,
              private storageService:StorageService) { }

  ngOnInit() {
    this.listarConvocatoriasVigentes()
  }
  listarConvocatoriasVigentes(){
    return this.listConvocatorias.getList().subscribe(
      res=>{
        console.log(res);
        
        if(res != "Empty"){
          this.convocatorias = res;
        }else{
          this.messgeAlert ="No existen convocatorias vigentes"
        }
      },
      error=> console.log(error)
    );    
  }
  goToItems(idConv:any){
    event.preventDefault();
    console.log("Usuarios de esta convocatoria con el id:"+idConv);
    //this.puenteService.sendObjectSource(idConv)
    this.storageService.store(AuthConstants.ID_CONV,idConv)
  }
}
