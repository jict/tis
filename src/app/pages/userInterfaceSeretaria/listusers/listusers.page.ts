import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { AuthConstants } from 'src/app/config/auth-constants';
import { ListinscriptionconvocatoryService } from 'src/app/services/listinscriptionconvocatory.service';
import { ListConvocatorysService } from 'src/app/services/list-convocatorys.service';
import { PuenteService } from 'src/app/services/puente.service';

@Component({
  selector: 'app-listusers',
  templateUrl: './listusers.page.html',
  styleUrls: ['./listusers.page.scss'],
})
export class ListusersPage implements OnInit {
  idConv:any
  listUsersConv:any
  messageEmptyList:any
  hiddenMessage:boolean = false;
  hiddenMessageOn:boolean = false;
  oneConvocatory:any
  constructor(private storageService:StorageService,
              private listUserInscription:ListinscriptionconvocatoryService,
              private getListOneConvocatoria:ListConvocatorysService,
              private puenteService:PuenteService) { }

  ngOnInit() {
    this.listarUsuariosConvocatoria()
    this.getConvocatoria()
  }
  listarUsuariosConvocatoria() {
    this.storageService.get(AuthConstants.ID_CONV).then(res => {
      this.idConv = res;
      console.log(this.idConv);
      return this.listUserInscription.getUsers(this.idConv).subscribe(
      res=>{
        console.log(res);
        if(res != "Empty"){
          this.listUsersConv = res
          this.hiddenMessageOn = true
        }else{
          this.messageEmptyList = "Ningun Postulante"
          this.hiddenMessage=true;
        }
      },error=>console.log(error)
    );
    })
  }
  getConvocatoria(){
    this.storageService.get(AuthConstants.ID_CONV).then(res => {
      this.idConv = res;
      console.log(this.idConv);
      return this.getListOneConvocatoria.getConvocatory(this.idConv).subscribe(
        res=>{
          this.oneConvocatory = res;
        },
        error=>console.log(error)
      )
    });
  }
  enviarNombre(user:any){
    this.puenteService.sendObjectSource(user);
  }
}
