import { Component, OnInit } from '@angular/core';
import { PuenteService } from 'src/app/services/puente.service';
import { ListitemsService } from 'src/app/services/listitems.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.page.html',
  styleUrls: ['./items.page.scss'],
})
export class ItemsPage implements OnInit {
  idConv:any;
  listaDeItems:any;
  rolComision:any;
  constructor(private puenteService:PuenteService,
              private items:ListitemsService){
    this.puenteService.$getObjectSource.subscribe((data:any)=>{
      this.idConv = data.id;
      this.rolComision = data.nombre_rol;
      //console.log(this.idConv)
    }).unsubscribe();
  }
  ngOnInit() {
    this.listarItems();
  }
  listarItems(){
    return this.items.getItems(this.idConv).subscribe(
      res=>{
        if(res=="Empty"){
          console.log(res);
        }else{
          this.listaDeItems=res;
          console.log(this.listaDeItems);
          
        }
      },error=>console.log(error)
    );
  }
  enviarIdItem(item){
    this.puenteService.sendObjectSource(item);
    this.puenteService.sendObjectSource2(this.rolComision);
  }
}
