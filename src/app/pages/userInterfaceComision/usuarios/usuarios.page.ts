import { Component, OnInit } from '@angular/core';
import { PuenteService } from 'src/app/services/puente.service';
import { ListreqService } from 'src/app/services/listreq.service';
import { ModalController } from '@ionic/angular';
import { ConocimientoPage } from './conocimiento/conocimiento.page';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.page.html',
  styleUrls: ['./usuarios.page.scss'],
})
export class UsuariosPage implements OnInit {
  idItem:any;
  idConv:any;
  listaDeUsuarios:any;
  messageEmpty:boolean=false;
  nombre_item:any;
  nombreRol:any;
  ruta:any;
  constructor(private puenteService:PuenteService,
              private usuarios:ListreqService,
              private modalController:ModalController){
    this.puenteService.$getObjectSource.subscribe((data:any)=>{
      this.idItem = data.id;
      this.idConv = data.id_conv;
      this.nombre_item=data.nombre_item;
      console.log(this.idConv)
      console.log(this.idItem)
    }).unsubscribe();
    this.puenteService.$getObjectSource2.subscribe((data:any)=>{
      this.nombreRol=data;
      console.log(this.nombreRol)
    }).unsubscribe();
  }

  ngOnInit() {
    this.listarUsuarios();
  }
  listarUsuarios(){
    return this.usuarios.listarUsuariosPorItem(this.idConv,this.idItem).subscribe(
      res=>{
        if(res=="Empty"){
          console.log(res);
          this.messageEmpty=true;
        }else{
          this.listaDeUsuarios=res;
          console.log(this.listaDeUsuarios);
        }
      },error=>console.log(error)
    );
  }
  goToEvaluation(usuario){
    switch(this.nombreRol){
      case 'Conocimiento':
        this.puenteService.sendObjectSource(usuario);
        this.ruta="conocimiento"
        break;
        case 'Meritos':
          this.puenteService.sendObjectSource(usuario);
          this.ruta="meritos"
          break;
    }
  }
  async presentModal() {
    const modal = await this.modalController.create({
      component: ConocimientoPage,
      componentProps: {}
    });
    return await modal.present();
  }

}
