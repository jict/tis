import { Component, OnInit } from '@angular/core';
import { PuenteService } from 'src/app/services/puente.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TematicaService } from 'src/app/services/tematica.service';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-conocimiento',
  templateUrl: './conocimiento.page.html',
  styleUrls: ['./conocimiento.page.scss'],
})
export class ConocimientoPage implements OnInit {
  idItem:any;
  idUser:any;
  nombre:any;

  notaTematicaForm:FormGroup;

  listaDeNotas:any;
  listaDeTematicas:any;

  notaTotalDeUsuario:any;
  messgeAlert:any;
  constructor(private puenteService:PuenteService,
              private formBuilder:FormBuilder,
              private tematica:TematicaService,
              private alertCtrl:AlertController,
              private router:Router) {
      this.buildFormNota();
      this.puenteService.$getObjectSource.subscribe((data:any)=>{
      this.idItem = data.id_item;
      this.idUser = data.id_user;
      this.notaTematicaForm.controls['idUser'].setValue(this.idUser);
      this.notaTematicaForm.controls['idItem'].setValue(this.idItem);
      this.nombre = data.first_name+' '+data.last_namep+' '+data.last_namem;
      console.log(this.idUser)
      console.log(this.idItem)
    }).unsubscribe();
   }

  ngOnInit() {
    this.listarTematicas();
    this.listarNotasDeUsuario();
    this.obtenerNotaTotal();
  }
  listarTematicas() {
    return this.tematica.listarTematicas(this.idItem).subscribe(
      res=>{
        if(res == "Empty"){
          console.log(res);
        }else{
          this.listaDeTematicas = res;
          console.log(this.listaDeTematicas);
        }
      },error=>console.log(error)
    );
  }
  listarNotasDeUsuario(){
    return this.tematica.listarNotaTematica(this.idUser, this.idItem).subscribe(
      res=>{
        if(res =="Empty"){
          console.log(res);
          //this.listaDeNotas = this.listaDeRendimientos;
        }else{
          this.listaDeNotas = res;
          console.log(this.listaDeNotas);
        }
      },error=>console.log(error)
      
    );
  }
  obtenerNotaTotal(){
    return this.tematica.sumarNotaUsuarioTematica(this.idUser,this.idItem).subscribe(
      res=>{
        if(res == "Empty"){
          console.log(res);
        }else{
          this.notaTotalDeUsuario = res;
        }
      },error=>console.log(error)
    );
  }
  private buildFormNota() {
    this.notaTematicaForm = this.formBuilder.group({
      idUser:[''],
      idItem:[''],
      idTematica:[''],
      nota:['',[Validators.required, Validators.min(0),Validators.max(100)]],
    });
  }
  asignarNota(idTematica){
    this.notaTematicaForm.controls['idTematica'].setValue(idTematica);
    console.log(this.notaTematicaForm.value);
    return this.tematica.modificarNotaTematica(this.notaTematicaForm.value).subscribe(
      res=>{
        if(res=="Succes"){
          this.messgeAlert="Nota asignada correctamente"
          this.notaTematicaForm.controls['nota'].reset();
          this.alert();
          this.doRefresh();
        }else{
          if(res=="Fail"){
            this.messgeAlert="La nota que quiere asignar es mayor a la Maxima Permitida"
            this.alert();
          }else{
            this.messgeAlert="El Postulante ya esta evaluado en esta Tematica"
            this.alert();
          }
        }
      },error=>console.log(error)
    );
  }

  async alert() {
    const alert = await this.alertCtrl.create({
      message: this.messgeAlert,
      buttons: ['OK']
    });

    await alert.present();
  }
  doRefresh() {
    console.log('Begin async operation');

    setTimeout(() => {
      this.ngOnInit()
      //event.target.complete();
    }, 2000);
  }

  goToPostulantes(){
    this.router.navigate(['/home/convocatorias/items-comision/usuarios']);
  }
}
