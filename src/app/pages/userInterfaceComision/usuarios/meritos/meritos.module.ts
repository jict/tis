import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MeritosPageRoutingModule } from './meritos-routing.module';

import { MeritosPage } from './meritos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MeritosPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [MeritosPage]
})
export class MeritosPageModule {}
