import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConvocatoriasPage } from './convocatorias.page';

const routes: Routes = [
  {
    path: '',
    component: ConvocatoriasPage
  },
  {
    path: 'items-comision',
    loadChildren: () => import('../../userInterfaceComision/items/items.module').then( m => m.ItemsPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConvocatoriasPageRoutingModule {}
