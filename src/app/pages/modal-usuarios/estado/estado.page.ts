import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ReqOptionService } from 'src/app/services/req-option.service';
import { PuenteService } from 'src/app/services/puente.service';
import { TematicaService } from 'src/app/services/tematica.service';
import { ListitemsService } from 'src/app/services/listitems.service';
import { NotasPage } from '../notas/notas.page';

@Component({
  selector: 'app-estado',
  templateUrl: './estado.page.html',
  styleUrls: ['./estado.page.scss'],
})
export class EstadoPage implements OnInit {
  nombre;
  idConv;
  listaDeSecciones:Array<any>;
  idUser;
  notaTotalDeUsuario:number;
  puntosTotalDeUsuario:number;
  data:any;
  listaDeItemsInscritos:Array<any>;
  idInscri:any;
  constructor(private modalCtrl:ModalController,
              private meritos:ReqOptionService,
              private puenteService:PuenteService,
              private items:ListitemsService) {
                this.puenteService.$getObjectSource.subscribe((data:any)=>{
                  this.nombre = data.first_name+' '+data.last_namep+' '+data.last_namem;
                  this.idConv = data.id_conv;
                  this.idUser = data.id;
                  this.data = data;
                }).unsubscribe();
  }
  ngOnInit() {
    this.listarSecciones();
    this.listarNotaTotalRendimiento();
    this.listarPuntosTotalExperiencia();
    this.listarItemsInscritas();
  }
  listarSecciones(){
    return this.meritos.listarSecciones().subscribe(
      res=>{
        if(res == "Empty"){
          console.log(res);
        }else{
          this.listaDeSecciones = res;
        }
      },error=> console.log(error)
      
    );
  }
  listarNotaTotalRendimiento(){
    return this.meritos.sumarNotaUsuario(this.idUser,this.idConv).subscribe(
      res=>{
        if(res == "Empty"){
          console.log(res);
        }else{
          this.notaTotalDeUsuario = parseInt(res[0]);
          console.log(this.notaTotalDeUsuario)
        }
      },error=>console.log(error)
    );
  }
  listarPuntosTotalExperiencia(){
    this.meritos.getIdInscri(this.idUser,this.idConv).subscribe(
      data=>{
        console.log(data);
        this.idInscri = data[0].id;
        console.log("El id de inscri es " + this.idInscri );
        this.meritos.sumarPuntosUsuario(this.idInscri).subscribe(
          res=>{
            if(res == "Empty"){
              console.log(res);
            }else{
              this.puntosTotalDeUsuario = parseInt(res[0]);
              console.log(this.puntosTotalDeUsuario)
            }
          },error=>console.log(error)
        );
      }
    );
  }
  listarItemsInscritas(){
    return this.items.listarItemsInscritos(this.idUser,this.idConv).subscribe(
      res=>{
        if(res == "Empty"){
          console.log(res);
        }else{
          this.listaDeItemsInscritos = res;
        }
      },error=>console.log(error)
    );
  }
  async presentModalMeritos(condicion){
    this.puenteService.sendObjectSource(this.data); 
    const modal = await this.modalCtrl.create({
      component: NotasPage,
      componentProps: {
        'condicion':condicion
      }
    });
    return await modal.present();

  }
  async presentModalConocimiento(item,condicion,notaFinalMeritos){
    console.log(item);
    const modal = await this.modalCtrl.create({
      component: NotasPage,
      componentProps: {
        'condicion':condicion,
        'notaMeritos':notaFinalMeritos,
        'idItem':item
      }
    });
    return await modal.present();

  }
  dismiss(){
    this.modalCtrl.dismiss();
  }
}
