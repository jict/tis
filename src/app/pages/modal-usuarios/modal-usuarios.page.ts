import { Component, OnInit, Input } from '@angular/core';
import { ListreqService } from 'src/app/services/listreq.service';
import { ModalController } from '@ionic/angular';
import { EstadoPage } from './estado/estado.page';
import { PuenteService } from 'src/app/services/puente.service';

@Component({
  selector: 'app-modal-usuarios',
  templateUrl: './modal-usuarios.page.html',
  styleUrls: ['./modal-usuarios.page.scss'],
})
export class ModalUsuariosPage implements OnInit {
  @Input() condicion;
  @Input() idConvocatoria;

  mensaje:String;
  ocultarMensaje:boolean;
  listaDePostulantes:Array<any>
  condicionBoton:boolean;
  constructor(private postulante:ListreqService,
              private modalCtrl:ModalController,
              private puenteService:PuenteService) { }

  ngOnInit() {
    this.listarPostulantes();
  }
  listarPostulantes(){
    switch(this.condicion){
      case 'Habilitados': 
        return this.postulante.listarHabilitados(this.idConvocatoria).subscribe(
          res=>{
            if( res == "Empty"){
              this.mensaje = "No existen postulantes inscritos o habilitados";
              this.ocultarMensaje = false;
              console.log(res);
              
            }else{
              this.ocultarMensaje = true;
              this.listaDePostulantes = res;
              console.log(this.listaDePostulantes);
            }
          },error=>console.log(error)
        );
      break;
      case 'Inhabilitados': 
      return this.postulante.listarInHabilitados(this.idConvocatoria).subscribe(
        res=>{
          if( res == "Empty"){
            this.mensaje = "No existen postulantes inscritos o inhabilitados";
            this.ocultarMensaje = false;
            this.condicionBoton = false;
            console.log(res);
          }else{
            this.ocultarMensaje = true;
            this.listaDePostulantes = res;
            console.log(this.listaDePostulantes);
            this.condicionBoton = true;
          }
        },error=>console.log(error)
      );
      break;
    }
  }
  async presentModal(postulante){
    this.puenteService.sendObjectSource(postulante);    
    const modal = await this.modalCtrl.create({
      component: EstadoPage,
      componentProps: {}
    });
    return await modal.present();
  }
  dismiss(){
    this.modalCtrl.dismiss();
  }
}
