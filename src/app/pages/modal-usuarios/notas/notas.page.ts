import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PuenteService } from 'src/app/services/puente.service';
import { ReqOptionService } from 'src/app/services/req-option.service';
import { TematicaService } from 'src/app/services/tematica.service';

@Component({
  selector: 'app-notas',
  templateUrl: './notas.page.html',
  styleUrls: ['./notas.page.scss'],
})
export class NotasPage implements OnInit {
  @Input()condicion;
  @Input()notaMeritos;
  @Input()idItem;
  idConv;
  idUser;
  idInscri;
  listaDeDatos:Array<any>;
  hiddenRendimiento:boolean = false;
  hiddenExperiencia:boolean = false;
  hiddenTematica:boolean = false;
  notaFinalTematica:number;

  listaDeReqOpcionales:any;
  constructor(private modalCtrl:ModalController,
              private puenteService:PuenteService,
              private rendimiento:ReqOptionService,
              private tematica:TematicaService) {
    this.puenteService.$getObjectSource.subscribe((data:any)=>{
      this.idConv = data.id_conv;
      this.idUser = data.id;
      console.log(this.idConv);
      console.log(this.idUser);
    }).unsubscribe();
  }

  ngOnInit() {
    this.listarDatos();
  }
  listarDatos(){
    switch(this.condicion){
      case 'Rendimiento':
        this.hiddenRendimiento = true;
        return this.rendimiento.listarNotas(this.idUser,this.idConv).subscribe(
          res=>{
            if(res == "Empty"){
              console.log(res);
            }else{
              this.listaDeDatos = res;
              console.log(this.listaDeDatos);
            }
          },error=>console.log(error)
        );
      break;
      case'Experiencia':
      this.hiddenExperiencia=true;
      this.rendimiento.getIdInscri(this.idUser,this.idConv).subscribe(
        data=>{
          console.log(data);
          this.idInscri = data[0].id;
          console.log("El id de inscri es " + this.idInscri );
          this.rendimiento.listarDocumentosExGeneral(this.idInscri).subscribe(
            res=>{
              if( res == "Empty"){
                console.log(res);
              }else{
                this.listaDeReqOpcionales = res;
                console.log(res);
                
              }
            },error=>console.log(error)
            
            );
          }
      );
      break;
      case'Tematica':
      this.hiddenTematica=true;
        return this.tematica.listarNotaTematica(this.idUser,this.idItem).subscribe(
          res=>{
            if(res == "Empty"){
              console.log(res);
            }else{
              this.listaDeDatos = res;
              console.log(this.listaDeDatos);
              this.obtenerNotaTotalTematica();
            }
          },error=>console.log(error)
        );
      break;
    }
  }
  obtenerNotaTotalTematica(){
    return this.tematica.sumarNotaUsuarioTematica(this.idUser,this.idItem).subscribe(
      res=>{
        if(res == "Empty"){
          console.log(res);
        }else{
          this.notaFinalTematica = parseInt(res[0]);
          console.log(this.notaFinalTematica);
        }
      },error=>console.log(error)
    );
  }
  dismiss(){
    this.hiddenRendimiento = false;
    this.hiddenExperiencia=false;
    this.hiddenTematica=false;
    this.modalCtrl.dismiss();
  }
}
