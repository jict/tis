export class AuthConstants {
  public static readonly AUTH = 'authToken';
  public static readonly TYPE_USER = 'typeUser';
  public static readonly ID_CONV = 'idConv';
}