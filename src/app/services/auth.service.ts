import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  authUser(loginForm: object): Observable<any> {
    return this.httpService.post('auntentificar', loginForm);
  }
}
