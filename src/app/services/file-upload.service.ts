import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  constructor(
    private httpService: HttpService
  ) { }

  getIdInscConv(postData: any): Observable<any> {
    return this.httpService.post('getidinscconv', postData);
  }

  setFileDir(postData: any): Observable<any> {
    return this.httpService.post('setFileDir', postData);
  }
}
