import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class ListConvocatorysService {

  constructor(private httpService:HttpService) { }

  getList():Observable<any>{
    return this.httpService.get('listConvocatorys')
  }
  getConvocatory(idConv:any):Observable<any>{
    return this.httpService.get('getConvocatory/'+idConv)
  }
  listarConvocatoriasComision(idUser:any):Observable<any>{
    return this.httpService.get('listarConvocatoriasComision/'+idUser)
  }
  listarConvocatoriasVigentes():Observable<any>{
    return this.httpService.get('listarConvocatoriasVigentes')
  }
}
