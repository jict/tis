import { TestBed } from '@angular/core/testing';

import { ListitemsService } from './listitems.service';

describe('ListitemsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListitemsService = TestBed.get(ListitemsService);
    expect(service).toBeTruthy();
  });
});
