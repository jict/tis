import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegisterUserService {

  constructor(
    private httpService: HttpService
  ) { }

  registerUser(sigupForm: Object): Observable<any> {
    return this.httpService.post('registrar', sigupForm);
  }
}
