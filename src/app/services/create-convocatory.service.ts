import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class CreateConvocatoryService {

  constructor(private httpService:HttpService) { }

  create(convocatoryForm: Object):Observable<any>{

    return this.httpService.post('createConvocatory', convocatoryForm)
  }

}