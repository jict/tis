import { TestBed } from '@angular/core/testing';

import { CreateConvocatoryService } from './create-convocatory.service';

describe('CreateConvocatoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CreateConvocatoryService = TestBed.get(CreateConvocatoryService);
    expect(service).toBeTruthy();
  });
});
