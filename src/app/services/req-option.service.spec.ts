import { TestBed } from '@angular/core/testing';

import { ReqOptionService } from './req-option.service';

describe('ReqOptionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReqOptionService = TestBed.get(ReqOptionService);
    expect(service).toBeTruthy();
  });
});
