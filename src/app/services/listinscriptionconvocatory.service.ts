import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class ListinscriptionconvocatoryService {
  id:any
  constructor(private httpService:HttpService) { }
  getList(data:object):Observable<any>{
    this.id = data    
    return this.httpService.get('listConvoPostulaciones/'+this.id)
  }
  getUsers(idConv:object):Observable<any>{
    return this.httpService.get('listUserConvocatory/'+idConv)
  }
  deleteInscription(idUser:object):Observable<any>{
    return this.httpService.post('deleteInscription',idUser)
  }
}
