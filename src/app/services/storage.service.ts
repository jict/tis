import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  async store(storagekey: string, value: any) {
    const encryptedValue = btoa(escape(JSON.stringify(value)));
    await localStorage.setItem(
      storagekey,
      encryptedValue
    );
  }

  async get(storageKey: string) {
    const ret = await localStorage.getItem(
      storageKey
    );
    return JSON.parse(unescape(atob(ret)));
  }

  async remove(storageKey: string) {
    await localStorage.removeItem(
      storageKey
    );
  }

  async clear() {
    await localStorage.clear();
  }
}
