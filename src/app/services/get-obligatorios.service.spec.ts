import { TestBed } from '@angular/core/testing';

import { GetObligatoriosService } from './get-obligatorios.service';

describe('GetObligatoriosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetObligatoriosService = TestBed.get(GetObligatoriosService);
    expect(service).toBeTruthy();
  });
});
