import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class ReqOptionService {

  constructor(private httpService:HttpService) { }

  addReqOption(reqForm:any):Observable<any>{
    return this.httpService.post('addReqOpt', reqForm)
  }
  listReqOption(idConv:any):Observable<any>{
    return this.httpService.get('listReqConvocatory/'+ idConv)
  }
  delReqOption(reqForm:any):Observable<any>{
    return this.httpService.post('deleteReqOpt', reqForm)
  }
  guardarRendimiento(rendimientoForm:any):Observable<any>{
    return this.httpService.post('rendimiento',rendimientoForm)
  }
  listarRendimientos(idConv:any):Observable<any>{
    return this.httpService.get('listarRendimientoAcademico/'+ idConv)
  }
  eliminarRendimiento(rendimientoForm:any):Observable<any>{
    return this.httpService.post('eliminarRendimiento', rendimientoForm)
  }
  añadirSeccion(seccionForm:any):Observable<any>{
    return this.httpService.post('aniadirSecciones', seccionForm)
  }
  listarSecciones():Observable<any>{
    return this.httpService.get('listarSecciones')
  }

  listarPuntosSecciones(idConv:any):Observable<any>{
    return this.httpService.get('listarPuntosSecciones/'+idConv)
  }

  sumarSecciones(idConv:any):Observable<any>{
    return this.httpService.get('sumaSecciones/'+idConv)
  }
  sumarRendimiento(idConv:any):Observable<any>{
    return this.httpService.get('sumaRendimiento/'+idConv)
  }
  sumarExperiencia(idConv:any):Observable<any>{
    return this.httpService.get('sumaExperiencia/'+idConv)
  }
  
  modificarNotaRendimiento(notaRendimientoForm:any):Observable<any>{
    return this.httpService.post('aniadirNotaRendimiento', notaRendimientoForm)
  }
  listarNotas(idUser:any,idConv:any):Observable<any>{
    return this.httpService.get('listarNotaRendimiento/'+idUser+'/'+idConv)
  }
  sumarNotaUsuario(idUser:any,idConv:any):Observable<any>{
    return this.httpService.get('sumarNotas/'+idUser+'/'+idConv)
  }

  modificarPuntoExperiencia(puntoExperienciaForm:any):Observable<any>{
    return this.httpService.post('aniadirPuntosExperiencia', puntoExperienciaForm)
  }
  listarPuntos(idUser:any,idConv:any):Observable<any>{
    return this.httpService.get('listarPuntosExperiencia/'+idUser+'/'+idConv)
  }
  sumarPuntosUsuario(idInscri:any):Observable<any>{
    return this.httpService.get('sumarPuntos/'+idInscri)
  }

  getIdInscri(idUser:any,idConv:any):Observable<any>{
    return this.httpService.get('obtenerIdInscri/'+idUser+'/'+idConv)
  }


  listarDocumentosExGeneral(idInscr:any):Observable<any>{
    return this.httpService.get('listarRequisitosOpcional/'+idInscr)
  }
  listarDocumentosExGeneralPresentados(idInscr:any,nameReq:any):Observable<any>{
    return this.httpService.get('listarRequisitosOpcionalPresentados/'+idInscr+'/'+nameReq)
  }
  modificarPuntoExperienciaGeneral(fileDataForm:any):Observable<any>{
    return this.httpService.post('actualizarPuntosEXP', fileDataForm)
  }

}
