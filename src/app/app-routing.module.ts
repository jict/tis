import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  //{ path: '', redirectTo: 'inicio', pathMatch: 'full' },
  {
    path: '',
    loadChildren: () => import('./pages/index/index.module').then(m => m.IndexPageModule)
  },
  {
    path: 'inicio',
    loadChildren: () => import('./pages/index/index.module').then(m => m.IndexPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'convocatoria',
    loadChildren: () => import('./pages/convocatoria/convocatoria.module').then(m => m.ConvocatoriaPageModule)
  },
  {
    path: 'tabs',
    loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'createconvocatory',
    loadChildren: () => import('./pages/userInterfaceAdministrator/createconvocatory/createconvocatory.module').then(m => m.CreateconvocatoryPageModule)
  },
  {
    path: 'listconvocatoryAdmin',
    loadChildren: () => import('./pages/userInterfaceAdministrator/listconvocatory/listconvocatory.module').then(m => m.ListconvocatoryPageModule)
  },
  {
    path: 'createitem',
    loadChildren: () => import('./pages/userInterfaceAdministrator/createitem/createitem.module').then(m => m.CreateitemPageModule)
  },
  {
    path: 'postulaciones',
    loadChildren: () => import('./pages/userInterfacePostulante/postulaciones/postulaciones.module').then(m => m.PostulacionesPageModule)
  },
  {
    path: 'listconvocatoryPost',
    loadChildren: () => import('./pages/userInterfacePostulante/listconvocatory/listconvocatory.module').then(m => m.ListconvocatoryPageModule)
  },
  {
    path: 'list-items',
    loadChildren: () => import('./pages/userInterfacePostulante/list-items/list-items.module').then(m => m.ListItemsPageModule)
  },
  {
    path: 'generatecode',
    loadChildren: () => import('./pages/userInterfaceSeretaria/generatecode/generatecode.module').then(m => m.GeneratecodePageModule)
  },
  {
    path: 'listconvocatorias',
    loadChildren: () => import('./pages/userInterfaceSeretaria/listconvocatorias/listconvocatorias.module').then(m => m.ListconvocatoriasPageModule)
  },
  {
    path: 'listusers',
    loadChildren: () => import('./pages/userInterfaceSeretaria/listusers/listusers.module').then(m => m.ListusersPageModule)
  },
  {
    path: 'public-convocatory',
    loadChildren: () => import('./pages/userInterfaceSeretaria/public-convocatory/public-convocatory.module').then(m => m.PublicConvocatoryPageModule)
  },
  {
    path: 'modal-participar',
    loadChildren: () => import('./pages/modal-participar/modal-participar.module').then(m => m.ModalParticiparPageModule)
  },
  {
    path: 'requirements',
    loadChildren: () => import('./pages/userInterfaceAdministrator/requirements/requirements.module').then(m => m.RequirementsPageModule)
  },
  {
    path: 'reqoptions',
    loadChildren: () => import('./pages/userInterfaceAdministrator/requirements/reqoptions/reqoptions.module').then(m => m.ReqoptionsPageModule)
  },
  {
    path: 'req-opcionales',
    loadChildren: () => import('./pages/userInterfacePostulante/list-items/req-opcionales/req-opcionales.module').then(m => m.ReqOpcionalesPageModule)
  },
  {
    path: 'item',
    loadChildren: () => import('./pages/userInterfaceAdministrator/listconvocatory/item/item.module').then(m => m.ItemPageModule)
  },
  {
    path: 'tematica',
    loadChildren: () => import('./pages/userInterfaceAdministrator/listconvocatory/tematica/tematica.module').then(m => m.TematicaPageModule)
  },
  {
    path: 'comision-users',
    loadChildren: () => import('./pages/userInterfaceAdministrator/comision-users/comision-users.module').then(m => m.ComisionUsersPageModule)
  },
  {
    path: 'modal-asignacion',
    loadChildren: () => import('./pages/userInterfaceAdministrator/modal-asignacion/modal-asignacion.module').then(m => m.ModalAsignacionPageModule)
  },
  {
    path: 'convocatorias',
    loadChildren: () => import('./pages/userInterfaceComision/convocatorias/convocatorias.module').then(m => m.ConvocatoriasPageModule)
  },
  {
    path: 'items-comision',
    loadChildren: () => import('./pages/userInterfaceComision/items/items.module').then(m => m.ItemsPageModule)
  },
  {
    path: 'documentos',
    loadChildren: () => import('./pages/userInterfaceSeretaria/documentos/documentos.module').then(m => m.DocumentosPageModule)
  },
  {
    path: 'usuarios',
    loadChildren: () => import('./pages/userInterfaceComision/usuarios/usuarios.module').then(m => m.UsuariosPageModule)
  },
  {
    path: 'conf-meritos',
    loadChildren: () => import('./pages/userInterfaceAdministrator/requirements/conf-meritos/conf-meritos.module').then(m => m.ConfMeritosPageModule)
  },
  {
    path: 'meritos',
    loadChildren: () => import('./pages/userInterfaceComision/usuarios/meritos/meritos.module').then(m => m.MeritosPageModule)
  },
  {
    path: 'conocimiento',
    loadChildren: () => import('./pages/userInterfaceComision/usuarios/conocimiento/conocimiento.module').then(m => m.ConocimientoPageModule)
  },
  {
    path: 'modal-usuarios',
    loadChildren: () => import('./pages/modal-usuarios/modal-usuarios.module').then(m => m.ModalUsuariosPageModule)
  },  {
    path: 'modal-meritos',
    loadChildren: () => import('./pages/modal-meritos/modal-meritos.module').then( m => m.ModalMeritosPageModule)
  }


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, useHash: true })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
