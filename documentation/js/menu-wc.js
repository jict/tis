'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">borrar documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-44a95f3c4f858f8f88bf3cf82aaaaa67"' : 'data-target="#xs-components-links-module-AppModule-44a95f3c4f858f8f88bf3cf82aaaaa67"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-44a95f3c4f858f8f88bf3cf82aaaaa67"' :
                                            'id="xs-components-links-module-AppModule-44a95f3c4f858f8f88bf3cf82aaaaa67"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-44a95f3c4f858f8f88bf3cf82aaaaa67"' : 'data-target="#xs-injectables-links-module-AppModule-44a95f3c4f858f8f88bf3cf82aaaaa67"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-44a95f3c4f858f8f88bf3cf82aaaaa67"' :
                                        'id="xs-injectables-links-module-AppModule-44a95f3c4f858f8f88bf3cf82aaaaa67"' }>
                                        <li class="link">
                                            <a href="injectables/AuthService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AuthService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/HttpService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>HttpService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/RegisterUserService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>RegisterUserService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UsersService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UsersService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ComisionUsersPageModule.html" data-type="entity-link">ComisionUsersPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ComisionUsersPageModule-b2085230951febe34383510563f3b624"' : 'data-target="#xs-components-links-module-ComisionUsersPageModule-b2085230951febe34383510563f3b624"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ComisionUsersPageModule-b2085230951febe34383510563f3b624"' :
                                            'id="xs-components-links-module-ComisionUsersPageModule-b2085230951febe34383510563f3b624"' }>
                                            <li class="link">
                                                <a href="components/ComisionUsersPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ComisionUsersPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ComisionUsersPageRoutingModule.html" data-type="entity-link">ComisionUsersPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ComponentsModule.html" data-type="entity-link">ComponentsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ComponentsModule-2428771a46ba81693cc35e59c08e6ca7"' : 'data-target="#xs-components-links-module-ComponentsModule-2428771a46ba81693cc35e59c08e6ca7"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ComponentsModule-2428771a46ba81693cc35e59c08e6ca7"' :
                                            'id="xs-components-links-module-ComponentsModule-2428771a46ba81693cc35e59c08e6ca7"' }>
                                            <li class="link">
                                                <a href="components/FooterComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FooterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HeaderComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HeaderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NavBarComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NavBarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ConfMeritosPageModule.html" data-type="entity-link">ConfMeritosPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ConfMeritosPageModule-6dd23680fc561d8b849efaa045c5b1e1"' : 'data-target="#xs-components-links-module-ConfMeritosPageModule-6dd23680fc561d8b849efaa045c5b1e1"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ConfMeritosPageModule-6dd23680fc561d8b849efaa045c5b1e1"' :
                                            'id="xs-components-links-module-ConfMeritosPageModule-6dd23680fc561d8b849efaa045c5b1e1"' }>
                                            <li class="link">
                                                <a href="components/ConfMeritosPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConfMeritosPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ConfMeritosPageRoutingModule.html" data-type="entity-link">ConfMeritosPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ConocimientoPageModule.html" data-type="entity-link">ConocimientoPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ConocimientoPageModule-1b8c3e17e4dfe275a4cc4f3f54366860"' : 'data-target="#xs-components-links-module-ConocimientoPageModule-1b8c3e17e4dfe275a4cc4f3f54366860"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ConocimientoPageModule-1b8c3e17e4dfe275a4cc4f3f54366860"' :
                                            'id="xs-components-links-module-ConocimientoPageModule-1b8c3e17e4dfe275a4cc4f3f54366860"' }>
                                            <li class="link">
                                                <a href="components/ConocimientoPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConocimientoPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ConocimientoPageRoutingModule.html" data-type="entity-link">ConocimientoPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ConvocatoriaPageModule.html" data-type="entity-link">ConvocatoriaPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ConvocatoriaPageModule-d5f42400bb9e95815fdc844a527e33b1"' : 'data-target="#xs-components-links-module-ConvocatoriaPageModule-d5f42400bb9e95815fdc844a527e33b1"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ConvocatoriaPageModule-d5f42400bb9e95815fdc844a527e33b1"' :
                                            'id="xs-components-links-module-ConvocatoriaPageModule-d5f42400bb9e95815fdc844a527e33b1"' }>
                                            <li class="link">
                                                <a href="components/ConvocatoriaPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConvocatoriaPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ConvocatoriaPageRoutingModule.html" data-type="entity-link">ConvocatoriaPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ConvocatoriasPageModule.html" data-type="entity-link">ConvocatoriasPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ConvocatoriasPageModule-c9fbf834139e37d0c607552edd6ab63e"' : 'data-target="#xs-components-links-module-ConvocatoriasPageModule-c9fbf834139e37d0c607552edd6ab63e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ConvocatoriasPageModule-c9fbf834139e37d0c607552edd6ab63e"' :
                                            'id="xs-components-links-module-ConvocatoriasPageModule-c9fbf834139e37d0c607552edd6ab63e"' }>
                                            <li class="link">
                                                <a href="components/ConvocatoriasPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConvocatoriasPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ConvocatoriasPageRoutingModule.html" data-type="entity-link">ConvocatoriasPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CreateconvocatoryPageModule.html" data-type="entity-link">CreateconvocatoryPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CreateconvocatoryPageModule-5c04dbdd8e6a7d04bc72a58d99e9d06a"' : 'data-target="#xs-components-links-module-CreateconvocatoryPageModule-5c04dbdd8e6a7d04bc72a58d99e9d06a"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CreateconvocatoryPageModule-5c04dbdd8e6a7d04bc72a58d99e9d06a"' :
                                            'id="xs-components-links-module-CreateconvocatoryPageModule-5c04dbdd8e6a7d04bc72a58d99e9d06a"' }>
                                            <li class="link">
                                                <a href="components/CreateconvocatoryPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CreateconvocatoryPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CreateconvocatoryPageRoutingModule.html" data-type="entity-link">CreateconvocatoryPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CreateitemPageModule.html" data-type="entity-link">CreateitemPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CreateitemPageModule-862e2d2fad3f094ce6e28292cdaf155c"' : 'data-target="#xs-components-links-module-CreateitemPageModule-862e2d2fad3f094ce6e28292cdaf155c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CreateitemPageModule-862e2d2fad3f094ce6e28292cdaf155c"' :
                                            'id="xs-components-links-module-CreateitemPageModule-862e2d2fad3f094ce6e28292cdaf155c"' }>
                                            <li class="link">
                                                <a href="components/CreateitemPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CreateitemPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CreateitemPageRoutingModule.html" data-type="entity-link">CreateitemPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/DocumentosPageModule.html" data-type="entity-link">DocumentosPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-DocumentosPageModule-d9597d8ad1d1d0f8ebad89f5c01c991c"' : 'data-target="#xs-components-links-module-DocumentosPageModule-d9597d8ad1d1d0f8ebad89f5c01c991c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DocumentosPageModule-d9597d8ad1d1d0f8ebad89f5c01c991c"' :
                                            'id="xs-components-links-module-DocumentosPageModule-d9597d8ad1d1d0f8ebad89f5c01c991c"' }>
                                            <li class="link">
                                                <a href="components/DocumentosPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DocumentosPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/DocumentosPageRoutingModule.html" data-type="entity-link">DocumentosPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/EstadoPageModule.html" data-type="entity-link">EstadoPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-EstadoPageModule-4f1e22a415a55de0b7486d66923f9583"' : 'data-target="#xs-components-links-module-EstadoPageModule-4f1e22a415a55de0b7486d66923f9583"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-EstadoPageModule-4f1e22a415a55de0b7486d66923f9583"' :
                                            'id="xs-components-links-module-EstadoPageModule-4f1e22a415a55de0b7486d66923f9583"' }>
                                            <li class="link">
                                                <a href="components/EstadoPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">EstadoPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/EstadoPageRoutingModule.html" data-type="entity-link">EstadoPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/GeneratecodePageModule.html" data-type="entity-link">GeneratecodePageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-GeneratecodePageModule-3e1422d5f2a58c67a0d795833d3451c7"' : 'data-target="#xs-components-links-module-GeneratecodePageModule-3e1422d5f2a58c67a0d795833d3451c7"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-GeneratecodePageModule-3e1422d5f2a58c67a0d795833d3451c7"' :
                                            'id="xs-components-links-module-GeneratecodePageModule-3e1422d5f2a58c67a0d795833d3451c7"' }>
                                            <li class="link">
                                                <a href="components/GeneratecodePage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GeneratecodePage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/GeneratecodePageRoutingModule.html" data-type="entity-link">GeneratecodePageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/HomePageModule.html" data-type="entity-link">HomePageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-HomePageModule-8a141b1cb54532580f9f2ef819c415e6"' : 'data-target="#xs-components-links-module-HomePageModule-8a141b1cb54532580f9f2ef819c415e6"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-HomePageModule-8a141b1cb54532580f9f2ef819c415e6"' :
                                            'id="xs-components-links-module-HomePageModule-8a141b1cb54532580f9f2ef819c415e6"' }>
                                            <li class="link">
                                                <a href="components/HomePage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HomePage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/HomePageRoutingModule.html" data-type="entity-link">HomePageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/IndexPageModule.html" data-type="entity-link">IndexPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-IndexPageModule-82152f0d0f48a862ec6d22a2c50f69df"' : 'data-target="#xs-components-links-module-IndexPageModule-82152f0d0f48a862ec6d22a2c50f69df"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-IndexPageModule-82152f0d0f48a862ec6d22a2c50f69df"' :
                                            'id="xs-components-links-module-IndexPageModule-82152f0d0f48a862ec6d22a2c50f69df"' }>
                                            <li class="link">
                                                <a href="components/IndexPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">IndexPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/IndexPageRoutingModule.html" data-type="entity-link">IndexPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ItemPageModule.html" data-type="entity-link">ItemPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ItemPageModule-246c5ba44b8927d0278503967a65efb2"' : 'data-target="#xs-components-links-module-ItemPageModule-246c5ba44b8927d0278503967a65efb2"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ItemPageModule-246c5ba44b8927d0278503967a65efb2"' :
                                            'id="xs-components-links-module-ItemPageModule-246c5ba44b8927d0278503967a65efb2"' }>
                                            <li class="link">
                                                <a href="components/ItemPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ItemPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ItemPageRoutingModule.html" data-type="entity-link">ItemPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ItemsPageModule.html" data-type="entity-link">ItemsPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ItemsPageModule-460d9f8b88a8c561ca524e7657b7e93c"' : 'data-target="#xs-components-links-module-ItemsPageModule-460d9f8b88a8c561ca524e7657b7e93c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ItemsPageModule-460d9f8b88a8c561ca524e7657b7e93c"' :
                                            'id="xs-components-links-module-ItemsPageModule-460d9f8b88a8c561ca524e7657b7e93c"' }>
                                            <li class="link">
                                                <a href="components/ItemsPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ItemsPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ItemsPageRoutingModule.html" data-type="entity-link">ItemsPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ListconvocatoriasPageModule.html" data-type="entity-link">ListconvocatoriasPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ListconvocatoriasPageModule-5d8e0c209c36faa29afb9454ffc77228"' : 'data-target="#xs-components-links-module-ListconvocatoriasPageModule-5d8e0c209c36faa29afb9454ffc77228"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ListconvocatoriasPageModule-5d8e0c209c36faa29afb9454ffc77228"' :
                                            'id="xs-components-links-module-ListconvocatoriasPageModule-5d8e0c209c36faa29afb9454ffc77228"' }>
                                            <li class="link">
                                                <a href="components/ListconvocatoriasPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ListconvocatoriasPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ListconvocatoriasPageRoutingModule.html" data-type="entity-link">ListconvocatoriasPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ListconvocatoryPageModule.html" data-type="entity-link">ListconvocatoryPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ListconvocatoryPageModule-3e021f2c7bda22fe1ba0f67f979bdfff"' : 'data-target="#xs-components-links-module-ListconvocatoryPageModule-3e021f2c7bda22fe1ba0f67f979bdfff"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ListconvocatoryPageModule-3e021f2c7bda22fe1ba0f67f979bdfff"' :
                                            'id="xs-components-links-module-ListconvocatoryPageModule-3e021f2c7bda22fe1ba0f67f979bdfff"' }>
                                            <li class="link">
                                                <a href="components/ListconvocatoryPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ListconvocatoryPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ListconvocatoryPageModule.html" data-type="entity-link">ListconvocatoryPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ListconvocatoryPageModule-f6d21b50ec1aa67df4551c94c4d48de6-1"' : 'data-target="#xs-components-links-module-ListconvocatoryPageModule-f6d21b50ec1aa67df4551c94c4d48de6-1"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ListconvocatoryPageModule-f6d21b50ec1aa67df4551c94c4d48de6-1"' :
                                            'id="xs-components-links-module-ListconvocatoryPageModule-f6d21b50ec1aa67df4551c94c4d48de6-1"' }>
                                            <li class="link">
                                                <a href="components/ListconvocatoryPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ListconvocatoryPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ListconvocatoryPageRoutingModule.html" data-type="entity-link">ListconvocatoryPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ListconvocatoryPageRoutingModule.html" data-type="entity-link">ListconvocatoryPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ListItemsPageModule.html" data-type="entity-link">ListItemsPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ListItemsPageModule-e0c593cab23d10c999b72af6abde68cf"' : 'data-target="#xs-components-links-module-ListItemsPageModule-e0c593cab23d10c999b72af6abde68cf"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ListItemsPageModule-e0c593cab23d10c999b72af6abde68cf"' :
                                            'id="xs-components-links-module-ListItemsPageModule-e0c593cab23d10c999b72af6abde68cf"' }>
                                            <li class="link">
                                                <a href="components/ListItemsPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ListItemsPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ListItemsPageRoutingModule.html" data-type="entity-link">ListItemsPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ListusersPageModule.html" data-type="entity-link">ListusersPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ListusersPageModule-d0fc8ac58db796bbf75bd987c821caeb"' : 'data-target="#xs-components-links-module-ListusersPageModule-d0fc8ac58db796bbf75bd987c821caeb"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ListusersPageModule-d0fc8ac58db796bbf75bd987c821caeb"' :
                                            'id="xs-components-links-module-ListusersPageModule-d0fc8ac58db796bbf75bd987c821caeb"' }>
                                            <li class="link">
                                                <a href="components/ListusersPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ListusersPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ListusersPageRoutingModule.html" data-type="entity-link">ListusersPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/LoginPageModule.html" data-type="entity-link">LoginPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LoginPageModule-10048a1aee8aeb9365f3f63be500bc90"' : 'data-target="#xs-components-links-module-LoginPageModule-10048a1aee8aeb9365f3f63be500bc90"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LoginPageModule-10048a1aee8aeb9365f3f63be500bc90"' :
                                            'id="xs-components-links-module-LoginPageModule-10048a1aee8aeb9365f3f63be500bc90"' }>
                                            <li class="link">
                                                <a href="components/LoginPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LoginPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LoginPageRoutingModule.html" data-type="entity-link">LoginPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/MeritosPageModule.html" data-type="entity-link">MeritosPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-MeritosPageModule-108560a76f7855977cf0ea4d6236b93e"' : 'data-target="#xs-components-links-module-MeritosPageModule-108560a76f7855977cf0ea4d6236b93e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-MeritosPageModule-108560a76f7855977cf0ea4d6236b93e"' :
                                            'id="xs-components-links-module-MeritosPageModule-108560a76f7855977cf0ea4d6236b93e"' }>
                                            <li class="link">
                                                <a href="components/MeritosPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MeritosPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MeritosPageRoutingModule.html" data-type="entity-link">MeritosPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ModalAsignacionPageModule.html" data-type="entity-link">ModalAsignacionPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ModalAsignacionPageModule-a911c0a8d5bbb552c16a011fcd8b3b28"' : 'data-target="#xs-components-links-module-ModalAsignacionPageModule-a911c0a8d5bbb552c16a011fcd8b3b28"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ModalAsignacionPageModule-a911c0a8d5bbb552c16a011fcd8b3b28"' :
                                            'id="xs-components-links-module-ModalAsignacionPageModule-a911c0a8d5bbb552c16a011fcd8b3b28"' }>
                                            <li class="link">
                                                <a href="components/ModalAsignacionPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ModalAsignacionPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ModalAsignacionPageRoutingModule.html" data-type="entity-link">ModalAsignacionPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ModalMeritosPageModule.html" data-type="entity-link">ModalMeritosPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ModalMeritosPageModule-38c1210f2dae20913e1a64add74014ae"' : 'data-target="#xs-components-links-module-ModalMeritosPageModule-38c1210f2dae20913e1a64add74014ae"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ModalMeritosPageModule-38c1210f2dae20913e1a64add74014ae"' :
                                            'id="xs-components-links-module-ModalMeritosPageModule-38c1210f2dae20913e1a64add74014ae"' }>
                                            <li class="link">
                                                <a href="components/ModalMeritosPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ModalMeritosPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ModalMeritosPageRoutingModule.html" data-type="entity-link">ModalMeritosPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ModalPageModule.html" data-type="entity-link">ModalPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ModalPageModule-0f568110fedf5030f598bdbcb30b1420"' : 'data-target="#xs-components-links-module-ModalPageModule-0f568110fedf5030f598bdbcb30b1420"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ModalPageModule-0f568110fedf5030f598bdbcb30b1420"' :
                                            'id="xs-components-links-module-ModalPageModule-0f568110fedf5030f598bdbcb30b1420"' }>
                                            <li class="link">
                                                <a href="components/ModalPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ModalPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ModalPageRoutingModule.html" data-type="entity-link">ModalPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ModalParticiparPageModule.html" data-type="entity-link">ModalParticiparPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ModalParticiparPageModule-4a0a346988c5a87ddd351670bff5dea6"' : 'data-target="#xs-components-links-module-ModalParticiparPageModule-4a0a346988c5a87ddd351670bff5dea6"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ModalParticiparPageModule-4a0a346988c5a87ddd351670bff5dea6"' :
                                            'id="xs-components-links-module-ModalParticiparPageModule-4a0a346988c5a87ddd351670bff5dea6"' }>
                                            <li class="link">
                                                <a href="components/ModalParticiparPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ModalParticiparPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ModalParticiparPageRoutingModule.html" data-type="entity-link">ModalParticiparPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ModalUsuariosPageModule.html" data-type="entity-link">ModalUsuariosPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ModalUsuariosPageModule-04b6ee5e029383cb4ba9868c993874c7"' : 'data-target="#xs-components-links-module-ModalUsuariosPageModule-04b6ee5e029383cb4ba9868c993874c7"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ModalUsuariosPageModule-04b6ee5e029383cb4ba9868c993874c7"' :
                                            'id="xs-components-links-module-ModalUsuariosPageModule-04b6ee5e029383cb4ba9868c993874c7"' }>
                                            <li class="link">
                                                <a href="components/ModalUsuariosPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ModalUsuariosPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ModalUsuariosPageRoutingModule.html" data-type="entity-link">ModalUsuariosPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/NotasPageModule.html" data-type="entity-link">NotasPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-NotasPageModule-e956aa8810a6435836f576e71c1c343e"' : 'data-target="#xs-components-links-module-NotasPageModule-e956aa8810a6435836f576e71c1c343e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-NotasPageModule-e956aa8810a6435836f576e71c1c343e"' :
                                            'id="xs-components-links-module-NotasPageModule-e956aa8810a6435836f576e71c1c343e"' }>
                                            <li class="link">
                                                <a href="components/NotasPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NotasPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/NotasPageRoutingModule.html" data-type="entity-link">NotasPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PostulacionesPageModule.html" data-type="entity-link">PostulacionesPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PostulacionesPageModule-a8063250dbc41c537b4f0e22517594b4"' : 'data-target="#xs-components-links-module-PostulacionesPageModule-a8063250dbc41c537b4f0e22517594b4"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PostulacionesPageModule-a8063250dbc41c537b4f0e22517594b4"' :
                                            'id="xs-components-links-module-PostulacionesPageModule-a8063250dbc41c537b4f0e22517594b4"' }>
                                            <li class="link">
                                                <a href="components/PostulacionesPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PostulacionesPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PostulacionesPageRoutingModule.html" data-type="entity-link">PostulacionesPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PublicConvocatoryPageModule.html" data-type="entity-link">PublicConvocatoryPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PublicConvocatoryPageModule-921d81a13867c70b35e035e9e4c28260"' : 'data-target="#xs-components-links-module-PublicConvocatoryPageModule-921d81a13867c70b35e035e9e4c28260"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PublicConvocatoryPageModule-921d81a13867c70b35e035e9e4c28260"' :
                                            'id="xs-components-links-module-PublicConvocatoryPageModule-921d81a13867c70b35e035e9e4c28260"' }>
                                            <li class="link">
                                                <a href="components/PublicConvocatoryPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PublicConvocatoryPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PublicConvocatoryPageRoutingModule.html" data-type="entity-link">PublicConvocatoryPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ReqOpcionalesPageModule.html" data-type="entity-link">ReqOpcionalesPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ReqOpcionalesPageModule-13c7d118506bed9d9a5aab9a52ff9cf2"' : 'data-target="#xs-components-links-module-ReqOpcionalesPageModule-13c7d118506bed9d9a5aab9a52ff9cf2"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ReqOpcionalesPageModule-13c7d118506bed9d9a5aab9a52ff9cf2"' :
                                            'id="xs-components-links-module-ReqOpcionalesPageModule-13c7d118506bed9d9a5aab9a52ff9cf2"' }>
                                            <li class="link">
                                                <a href="components/ReqOpcionalesPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ReqOpcionalesPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ReqOpcionalesPageRoutingModule.html" data-type="entity-link">ReqOpcionalesPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ReqoptionsPageModule.html" data-type="entity-link">ReqoptionsPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ReqoptionsPageModule-429a9183f99b39ff0dc524f985a8d524"' : 'data-target="#xs-components-links-module-ReqoptionsPageModule-429a9183f99b39ff0dc524f985a8d524"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ReqoptionsPageModule-429a9183f99b39ff0dc524f985a8d524"' :
                                            'id="xs-components-links-module-ReqoptionsPageModule-429a9183f99b39ff0dc524f985a8d524"' }>
                                            <li class="link">
                                                <a href="components/ReqoptionsPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ReqoptionsPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ReqoptionsPageRoutingModule.html" data-type="entity-link">ReqoptionsPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/RequirementsPageModule.html" data-type="entity-link">RequirementsPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-RequirementsPageModule-c8b2bdaa06965f132f5d04997806ecd5"' : 'data-target="#xs-components-links-module-RequirementsPageModule-c8b2bdaa06965f132f5d04997806ecd5"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RequirementsPageModule-c8b2bdaa06965f132f5d04997806ecd5"' :
                                            'id="xs-components-links-module-RequirementsPageModule-c8b2bdaa06965f132f5d04997806ecd5"' }>
                                            <li class="link">
                                                <a href="components/RequirementsPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RequirementsPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RequirementsPageRoutingModule.html" data-type="entity-link">RequirementsPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SignupPageModule.html" data-type="entity-link">SignupPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SignupPageModule-1508eb63047313c506c6630af1dbe013"' : 'data-target="#xs-components-links-module-SignupPageModule-1508eb63047313c506c6630af1dbe013"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SignupPageModule-1508eb63047313c506c6630af1dbe013"' :
                                            'id="xs-components-links-module-SignupPageModule-1508eb63047313c506c6630af1dbe013"' }>
                                            <li class="link">
                                                <a href="components/SignupPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SignupPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SignupPageRoutingModule.html" data-type="entity-link">SignupPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/TabsPageModule.html" data-type="entity-link">TabsPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-TabsPageModule-a5d1f6a20075a3f5508af82c205b1c70"' : 'data-target="#xs-components-links-module-TabsPageModule-a5d1f6a20075a3f5508af82c205b1c70"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabsPageModule-a5d1f6a20075a3f5508af82c205b1c70"' :
                                            'id="xs-components-links-module-TabsPageModule-a5d1f6a20075a3f5508af82c205b1c70"' }>
                                            <li class="link">
                                                <a href="components/TabsPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TabsPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabsPageRoutingModule.html" data-type="entity-link">TabsPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/TematicaPageModule.html" data-type="entity-link">TematicaPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-TematicaPageModule-6dbbc0c32a16f1f1f9100c08a857ef31"' : 'data-target="#xs-components-links-module-TematicaPageModule-6dbbc0c32a16f1f1f9100c08a857ef31"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TematicaPageModule-6dbbc0c32a16f1f1f9100c08a857ef31"' :
                                            'id="xs-components-links-module-TematicaPageModule-6dbbc0c32a16f1f1f9100c08a857ef31"' }>
                                            <li class="link">
                                                <a href="components/TematicaPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TematicaPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TematicaPageRoutingModule.html" data-type="entity-link">TematicaPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/UsuariosPageModule.html" data-type="entity-link">UsuariosPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UsuariosPageModule-b009acb2cb3707b49d6d713df05e38ae"' : 'data-target="#xs-components-links-module-UsuariosPageModule-b009acb2cb3707b49d6d713df05e38ae"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UsuariosPageModule-b009acb2cb3707b49d6d713df05e38ae"' :
                                            'id="xs-components-links-module-UsuariosPageModule-b009acb2cb3707b49d6d713df05e38ae"' }>
                                            <li class="link">
                                                <a href="components/UsuariosPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UsuariosPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UsuariosPageRoutingModule.html" data-type="entity-link">UsuariosPageRoutingModule</a>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#components-links"' :
                            'data-target="#xs-components-links"' }>
                            <span class="icon ion-md-cog"></span>
                            <span>Components</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="components-links"' : 'id="xs-components-links"' }>
                            <li class="link">
                                <a href="components/ListconvocatoryPage-1.html" data-type="entity-link">ListconvocatoryPage</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/AppPage.html" data-type="entity-link">AppPage</a>
                            </li>
                            <li class="link">
                                <a href="classes/AuthConstants.html" data-type="entity-link">AuthConstants</a>
                            </li>
                            <li class="link">
                                <a href="classes/RedesSociales.html" data-type="entity-link">RedesSociales</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AuthService.html" data-type="entity-link">AuthService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CodigoService.html" data-type="entity-link">CodigoService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ComisionService.html" data-type="entity-link">ComisionService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CreateConvocatoryService.html" data-type="entity-link">CreateConvocatoryService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CreateItemService.html" data-type="entity-link">CreateItemService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FileUploadService.html" data-type="entity-link">FileUploadService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/GetObligatoriosService.html" data-type="entity-link">GetObligatoriosService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/HttpService.html" data-type="entity-link">HttpService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/InscriptionconvocatoyService.html" data-type="entity-link">InscriptionconvocatoyService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ListConvocatorysService.html" data-type="entity-link">ListConvocatorysService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ListinscriptionconvocatoryService.html" data-type="entity-link">ListinscriptionconvocatoryService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ListitemsService.html" data-type="entity-link">ListitemsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ListreqService.html" data-type="entity-link">ListreqService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PuenteService.html" data-type="entity-link">PuenteService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RegisterUserService.html" data-type="entity-link">RegisterUserService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ReqOptionService.html" data-type="entity-link">ReqOptionService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/StorageService.html" data-type="entity-link">StorageService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TematicaService.html" data-type="entity-link">TematicaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ToastService.html" data-type="entity-link">ToastService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UsersService.html" data-type="entity-link">UsersService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/Code.html" data-type="entity-link">Code</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Usuario.html" data-type="entity-link">Usuario</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});